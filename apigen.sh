#!/usr/bin/sh

while [[ $# == 1 ]]
do
  key="$1"

  case $key in
    -c|--clean)
      CLEAN=true
      shift
      ;;
  esac
  shift
done

if [ "$CLEAN" = true ] ; then
  if [[ -e 'api' ]] ; then
    rm -rf api
    rm -rf apigen.phar
  else
    echo "There is nothing to clean!"
  fi
else
  if ! [ -f apigen.phar ] ; then
    wget https://github.com/ApiGen/ApiGen.github.io/raw/master/apigen.phar | php
  fi

  php apigen.phar generate --source src --destination api/reference --template-theme bootstrap --deprecated \
  --download --tree --title="Scara MVC Reference"
fi
