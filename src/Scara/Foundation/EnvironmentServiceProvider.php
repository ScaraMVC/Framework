<?php

namespace Scara\Foundation;

use Scara\Support\ServiceProvider;

class EnvironmentServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function register()
    {
        $this->create('environment', function () {
            return new Environment();
        });
    }
}
