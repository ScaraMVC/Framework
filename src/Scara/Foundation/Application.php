<?php

namespace Scara\Foundation;

use Scara\Config\Configuration;
use Scara\Debug\ExceptionHandler;
use Scara\Hashing\OpenSslHasher;
use Scara\Http\Controller;
use Scara\Routing\Router;
use Scara\Session\SessionInitializer;
use Scara\Utils\Aliases;

/**
 * App Core. Responsible for running entire application.
 */
class Application
{
    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * The application router.
     *
     * @var \Scara\Routing\Router
     */
    private $_router;

    /**
     * The base controller.
     *
     * @var \Scara\Http\Controller
     */
    private $_controller;

    /**
     * The aliases class.
     *
     * @var \Scara\Utils\Aliases
     */
    private $_aliases;

    /**
     * Gets the current application version.
     *
     * @var string
     */
    public static $VERSION = '1.2';

    /**
     * Handles registering Scara's pre-execution status.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadConfig();
        $this->registerExceptionHandler();
        $this->registerProviders();
        $this->registerAliases();
        $this->registerScaraMasterSessionToken();
    }

    /**
     * The applications main entry point. Responsible for
     * executing the application.
     *
     * @return void
     */
    public function exec()
    {
        $this->registerRoutes();
        $this->registerControllers();
    }

    /**
     * Loads the application's main configuration.
     *
     * @return void
     */
    private function loadConfig()
    {
        $this->_config = new Configuration();
        $this->_config->from('app');

        // let's go ahead and set timezone
        date_default_timezone_set($this->_config->get('timezone'));
    }

    /**
     * Registers the applications debugging state.
     *
     * @return void
     */
    private function registerExceptionHandler()
    {
        new ExceptionHandler();
    }

    /**
     * Registers application routes.
     *
     * @return void
     */
    private function registerRoutes()
    {
        $this->_router = new Router();
        $this->_router->registerRoutes();
    }

    /**
     * Registers the aliases.
     *
     * @return void
     */
    private function registerAliases()
    {
        $this->_aliases = new Aliases();
        $this->_aliases->registerAliases();
    }

    /**
     * Register's the applications service providers.
     *
     * @return void
     */
    private function registerProviders()
    {
        foreach ($this->_config->get('services') as $service) {
            $class = new $service();
            $class->register();
        }
    }

    /**
     * Responsible for registering the app controllers.
     *
     * @return void
     */
    private function registerControllers()
    {
        $this->_controller = new Controller();
        $this->_controller->load($this->_router,
        $this->_config->get('basepath'));
    }

    /**
     * Registers Scara's master session.
     *
     * @return void
     */
    private function registerScaraMasterSessionToken()
    {
        $s = SessionInitializer::init();
        $h = new OpenSslHasher();

        if (!$s->has('SCARA_SESSION_TOKEN')) {
            $token = substr($h->tokenize($h->random_string(16), time()), 0, 16);
            $session = $h->tokenize($token, hash('sha256', $_SERVER['HTTP_USER_AGENT']));
            $s->set('SCARA_SESSION_TOKEN', $token);
            $s->set('SCARA_SESSION', $session);
        }
    }
}
