<?php

namespace Scara\Database;

use Closure;

/**
 * Table migration parent class.
 */
class Migration
{
    /**
     * Creates table from migration.
     *
     * @param string   $table
     * @param \Closure $closure
     *
     * @return void
     */
    public function create($table, Closure $closure)
    {
        $blueprint = $this->createBlueprint($table, $closure);
        $blueprint->create();
    }

    /**
     * updates table from migration.
     *
     * @param string   $table
     * @param \Closure $closure
     *
     * @return void
     */
    public function update($table, Closure $closure)
    {
        $blueprint = $this->createBlueprint($table, $closure);
        $blueprint->update();
    }

    /**
     * Drops table from migration.
     *
     * @param string $table
     *
     * @return void
     */
    public function drop($table)
    {
        $blueprint = $this->createBlueprint($table);
        $blueprint->drop();
    }

    /**
     * Creates a new blueprint instance.
     *
     * @param string   $table
     * @param \Closure $closure
     *
     * @return \Scara\Database\Blueprint
     */
    protected function createBlueprint($table, \Closure $closure = null)
    {
        return new Blueprint($table, $closure);
    }
}
