<?php

namespace Scara\Database;

/**
 * Migration blueprint class.
 */
class Blueprint
{
    /**
     * The table to manipulate.
     *
     * @var string
     */
    private $_table;

    /**
     * The table to manipulate.
     *
     * @var \Closure
     */
    private $_closure;

    /**
     * The table to manipulate.
     *
     * @var \Scara\Database\Database
     */
    private $_database;

    /**
     * The table to manipulate.
     *
     * @var \Illuminate\Database\Schema\Builder
     */
    private $_schema;

    /**
     * Class Constructor.
     *
     * @param string   $table
     * @param \Closure $closure
     */
    public function __construct($table, \Closure $closure = null)
    {
        $this->_table = $table;
        $this->_closure = $closure;

        $this->_database = new Database();
        $this->_schema = $this->_database->schema();
    }

    /**
     * Sends complete SQL script to Database for creation.
     *
     * @return void
     */
    public function create()
    {
        $this->_schema->create($this->_table, $this->_closure);
    }

    /**
     * Sends complete SQL script to Database for alteration.
     *
     * @return void
     */
    public function update()
    {
        $this->_schema->table($this->_table, $this->_closure);
    }

    /**
     * Sends complete SQL script to Database for truncation.
     *
     * @return void
     */
    public function drop()
    {
        $this->_schema->drop($this->_table);
    }
}
