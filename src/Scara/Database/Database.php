<?php

namespace Scara\Database;

use Illuminate\Database\Capsule\Manager as Capsule;
use Scara\Config\Configuration;

/**
 * Main Database connection class.
 */
class Database
{
    /**
     * Illuminate Capsule instance.
     *
     * @var \Illuminate\Database\Capsule\Manager
     */
    private $_capsule;

    /**
     * Capsule Schema instance.
     *
     * @var \Illuminate\Database\Schema\Builder
     */
    private $_schema;

    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Class Constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration();
        $this->_config = $c->from('database');

        // determine the driver being used and set database
        // name/file accordingly
        $database = '';
        switch ($this->_config->get('driver')) {
            case 'mysql':
            $database = $this->_config->get('name');
            break;
            case 'sqlite':
            $database = $this->_config->get('sqlite_file');
            break;
            default:
            $database = $this->_config->get('name');
            break;
        }

        $this->_capsule = new Capsule();
        $this->_capsule->addConnection([
            'driver'    => $this->_config->get('driver'),
            'host'      => $this->_config->get('host'),
            'database'  => $database,
            'username'  => $this->_config->get('username'),
            'password'  => $this->_config->get('password'),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => $this->_config->get('prefix'),
        ]);

        $this->_capsule->setAsGlobal();
        $this->_capsule->bootEloquent();
    }

    /**
     * Returns the Illuminate Capsule instance.
     *
     * @return \Illuminate\Database\Capsule\Manager
     */
    public function getCapsule()
    {
        return $this->_capsule;
    }

    /**
     * Returns the Capsule Schama instance.
     *
     * @return \Illuminate\Database\Schema\Builder
     */
    public function schema()
    {
        return Capsule::schema();
    }

    public function table($table)
    {
        $t = $this->getCapsule()->table($table);

        return $t;
    }
}
