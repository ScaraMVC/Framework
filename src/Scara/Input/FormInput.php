<?php

namespace Scara\Input;

/**
 * Gets form input from submitted form.
 */
class FormInput
{
    /**
     * Gets the requested form input.
     *
     * @param string $key - The input's key
     *
     * @return mixed
     */
    public function get($key)
    {
        if ($this->exists($key)) {
            return $this->input($key);
        }

        return false;
    }

    /**
     * Checks to see if an input exists.
     *
     * @param string $key - The input's key
     *
     * @return bool
     */
    public function exists($key)
    {
        if (!empty($this->input($key))) {
            return true;
        }

        return false;
    }

    /**
     * Gets all inputs.
     *
     * @return array
     */
    public function all()
    {
        return $this->input();
    }

    /**
     * Retrieves an input.
     *
     * @param null $key     - The input's key
     * @param null $default - The input $key's default value
     *
     * @return mixed
     */
    public function input($key = null, $default = null)
    {
        $input = $this->getInputSource();

        return $this->inputSift($input, $key, $default);
    }

    /**
     * Gets the source of the input.
     *
     * @return array
     */
    protected function getInputSource()
    {
        return $this->getMethod() == 'GET' ? $_GET : $_POST;
    }

    /**
     * Gets the request method.
     *
     * @return string
     */
    protected function getMethod()
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Sifts through the input array.
     *
     * @param array  $array   - The input array
     * @param string $key     - The input's key
     * @param string $default - The input $key's default value
     *
     * @return mixed
     */
    private function inputSift($array, $key, $default = null)
    {
        if (is_null($key)) {
            return $array;
        }

        if (isset($array[$key])) {
            return $array[$key];
        }

        foreach (explode('.', $key) as $segment) {
            if (!is_array($array) || !array_key_exists($segment, $array)) {
                return $default;
            }
            $array = $array[$segment];
        }

        return $array;
    }
}
