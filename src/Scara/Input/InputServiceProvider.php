<?php

namespace Scara\Input;

use Scara\Support\ServiceProvider;

/**
 * Input's Service provider.
 */
class InputServiceProvider extends ServiceProvider
{
    /**
     * Registers the input service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('input', function () {
            return new FormInput();
        });
    }
}
