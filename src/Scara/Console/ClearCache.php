<?php

namespace Scara\Console;

use Scara\Config\Configuration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Clears Views cache.
 */
class ClearCache extends Command
{
    private $_config;

    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('clear-cache')
        ->setDescription('Clears the cache');

        $c = new Configuration();
        $this->_config = $c->from('app');
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->clear($this->_config->get('cache'), $output);
        $this->clear($this->_config->get('session_location'), $output);
    }

    /**
     * Handles clearning out files from given location.
     *
     * @param string                                            $directory
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    private function clear($directory, OutputInterface $output)
    {
        $handle = opendir($directory);
        $ignore = ['.', '..', '.gitkeep'];
        while ($file = readdir($handle)) {
            if (!in_array($file, $ignore)) {
                $output->writeln("Clearing file {$file} from the cache");
                unlink($directory.$file);
            }
        }
    }
}
