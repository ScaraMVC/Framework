<?php

namespace Scara\Console\Database;

use Scara\Database\Database;
use Scara\Database\Migration;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Pushes migrations to the database.
 */
class Migrate extends Command
{
    /**
     * Database instance.
     *
     * @var \Scara\Database\Database
     */
    private $_db;

    /**
     * Database capsule instance.
     *
     * @var Illuminate\Database\Capsule\Manager
     */
    private $_cap;

    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:push')
        ->setDescription('Migrate database migrations')
        ->addArgument('migration', InputArgument::OPTIONAL, 'The migriation to push. Done by class name');

        $this->_db = new Database();
        $this->_cap = $this->_db->getCapsule();
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrationsTemplate = base_path().'/storage/migrations';
        $migration = $input->getArgument('migration');

        $file = file($migrationsTemplate);
        $items = '';
        $goahead = false;
        foreach ($file as $fn) {
            $item = $fn;
            $nameexp = explode('_', $item);

            for ($i = 0; $i < 4; $i++) {
                array_shift($nameexp);
            }

            foreach ($nameexp as $key => $i) {
                $nameexp[$key] = ucwords($i);
            }

            $name = str_replace('_', '', str_replace('.php', '', implode('_', $nameexp)));

            $path = base_path().'/database/'.$item;

            if (file_exists(trim($path))) {
                if (empty($migration)) {
                    $goahead = true;
                } else {
                    if ($migration == trim($name)) {
                        $goahead = true;
                    }
                }

                if ($goahead) {
                    // update migrations table
                    if ($this->isMigrated($item, $output)) {
                        $output->writeln('Item: '.trim($name).' is already pushed to the database');
                    } else {
                        $output->writeln('Migrating '.trim($name).' migration');
                        require_once trim($path);
                        $class = '\\'.trim($name);
                        $c = new $class();
                        $c->up();
                        $this->changeMigrationTable($item);
                    }
                }
            }
        }
    }

    /**
     * Update migrations table to reflect the migration push.
     *
     * @param string $migration
     *
     * @return void
     */
    private function changeMigrationTable($migration)
    {
        $table = $this->_cap->table('migrations');

        $dbm = $table->where('migration', '=', $migration);

        if ($dbm->count() > 0) {
            $item = $dbm->first();
            $item->updated_at = date('Y-m-d h:i:s');
            $item->migrated = 1;
            $table->update((array) $item);
        } else {
            $options['migration'] = $migration;
            $options['created_at'] = date('Y-m-d h:i:s');
            $options['updated_at'] = $options['created_at'];
            $options['migrated'] = 1;
            $table->insert($options);
        }
    }

    /**
     * Check whether a migration is in the database or not.
     *
     * @param string                                            $migration
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return bool
     */
    private function isMigrated($migration, OutputInterface $output = null)
    {
        if ($this->_db->schema()->hasTable('migrations')) {
            $table = $this->_cap->table('migrations');
            $m = $table->where('migration', '=', $migration);
            if ($m->count() > 0) {
                $item = $m->first();
                if ($item->migrated == 1) {
                    return true;
                }
            }
        } else {
            $this->createMigrationsTable($output);

            return $this->isMigrated($migration);
        }

        return false;
    }

    /**
     * Creates the migration table if it doesn't exist.
     *
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    private function createMigrationsTable(OutputInterface $output = null)
    {
        if (!is_null($output)) {
            $output->writeln('<info>Creating migrations table</info>');
        }
        $m = new CreateMigrationsTable();
        $m->up();
    }
}

/**
 * Base migration table migration.
 */
class CreateMigrationsTable extends Migration
{
    /**
     * For pushing migrations up.
     *
     * @return void
     */
    public function up()
    {
        $this->create('migrations', function ($table) {
            // Place table rows here
            $table->increments('id');
            $table->string('migration');
            $table->integer('migrated');
            $table->timestamps();
        });
    }

    /**
     * For reversing migrations // UNUSED.
     *
     * @return void
     */
    public function down()
    {
        // we're not removing the table...
    }
}
