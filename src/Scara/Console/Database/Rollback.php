<?php

namespace Scara\Console\Database;

use Scara\Database\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Migration rollback command.
 */
class Rollback extends Command
{
    /**
     * Database instance.
     *
     * @var \Scara\Database\Database
     */
    private $_db;

    /**
     * Database capsule instance.
     *
     * @var Illuminate\Database\Capsule\Manager
     */
    private $_cap;

    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:rollback')
        ->setDescription('Rollback migrations')
        ->addArgument('migration', InputArgument::OPTIONAL, 'The migriation to rollback. Done by class name');

        $this->_db = new Database();
        $this->_cap = $this->_db->getCapsule();
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrationsTemplate = base_path().'/storage/migrations';
        $migration = $input->getArgument('migration');

        $file = file($migrationsTemplate);
        $items = '';
        $goahead = false;
        foreach ($file as $fn) {
            $item = $fn;
            $nameexp = explode('_', $item);

            for ($i = 0; $i < 4; $i++) {
                array_shift($nameexp);
            }

            foreach ($nameexp as $key => $i) {
                $nameexp[$key] = ucwords($i);
            }

            $name = str_replace('_', '', str_replace('.php', '', implode('_', $nameexp)));

            $path = base_path().'/database/'.$item;

            if (file_exists(trim($path))) {
                if (empty($migration)) {
                    $goahead = true;
                } else {
                    if ($migration == trim($name)) {
                        $goahead = true;
                    }
                }

                if ($goahead) {
                    // update migrations table
                    if ($this->isMigrated($item)) {
                        $output->writeln('Rolling back '.trim($name).' migration');
                        require_once trim($path);
                        $class = '\\'.trim($name);
                        $c = new $class();
                        $c->down();
                        $this->changeMigrationTable($item);
                    } else {
                        $output->writeln('Item: '.trim($name).' is already pulled from the database');
                    }
                }
            }
        }
    }

    /**
     * Update migrations table to reflect the migration push.
     *
     * @param string $migration
     *
     * @return void
     */
    private function changeMigrationTable($migration)
    {
        $table = $this->_cap->table('migrations');

        $dbm = $table->where('migration', '=', $migration);

        if ($dbm->count() > 0) {
            $item = $dbm->first();
            $item->updated_at = date('Y-m-d h:i:s');
            $item->migrated = 0;
            $table->update((array) $item);
        } else {
            $options['migration'] = $migration;
            $options['created_at'] = date('Y-m-d h:i:s');
            $options['updated_at'] = $options['created_at'];
            $options['migrated'] = 1;
            $table->insert($options);
        }
    }

    /**
     * Check whether a migration is in the database or not.
     *
     * @param string $migration
     *
     * @return bool
     */
    private function isMigrated($migration)
    {
        if ($this->_db->schema()->hasTable('migrations')) {
            $table = $this->_cap->table('migrations');
            $m = $table->where('migration', '=', $migration);
            if ($m->count() > 0) {
                $item = $m->first();
                if ($item->migrated == 1) {
                    return true;
                }
            }
        } else {
            $this->createMigrationsTable();

            return $this->isMigrated($migration);
        }

        return false;
    }
}
