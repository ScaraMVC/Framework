<?php

namespace Scara\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Runs a database seed.
 */
class Seed extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:seed')
        ->setDescription('Executes a database seed')
        ->addArgument('seed', InputArgument::REQUIRED, 'The seed to run');
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $seed = $input->getArgument('seed');

        $seedPath = base_path().'/database/seeds';
        $file = $seedPath.'/'.$seed.'.php';

        if (file_exists($file)) {
            require_once $file;
            $class = '\\'.$seed;
            $c = new $class();
            $c->run();
        } else {
            $output->writeln("<error>$seed seed does not exist!</error>");
        }
    }
}
