<?php

namespace Scara\Console\Database;

use Scara\Database\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Truncates a given table.
 */
class Truncate extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:truncate')
        ->setDescription('Truncate database table')
        ->addArgument('table', InputArgument::REQUIRED, 'The table to truncate');
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $table = $input->getArgument('table');

        $db = new Database();
        $cap = $db->getCapsule();
        $sch = $db->schema();

        if ($sch->hasTable($table)) {
            $t = $cap->table($table);
            if ($t->count() > 0) {
                $t->truncate();

                $output->writeln("<info>Table \"$table\" was successfully truncated</info>");
            } else {
                $output->writeln("<error>The table is empty, and therefore doesn't need to be truncated</error>");
            }
        } else {
            $output->writeln("<error>The table \"$table\" does not exist!</error>");
        }
    }
}
