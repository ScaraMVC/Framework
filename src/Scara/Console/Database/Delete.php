<?php

namespace Scara\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Deletes a given migration.
 */
class Delete extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:delete')
        ->setDescription('Deletes a migration and runs it\'s down() command')
        ->setDefinition(new InputDefinition([
            new InputArgument('migration', InputArgument::REQUIRED),
        ]));
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $migrationsTemplate = base_path().'/storage/migrations';
        $migration = $input->getArgument('migration');

        $file = file($migrationsTemplate);
        foreach ($file as $item) {
            $itemexp = explode('_', str_replace('.php', '', $item));

            for ($i = 0; $i < 4; $i++) {
                array_shift($itemexp);
            }

            $name = trim(implode('_', $itemexp));

            if ($migration == $name) {
                $path = trim(base_path().'/database/'.$item);
                if (file_exists($path)) {
                    $output->writeln('<info>Rolling back migration</info>');
                    foreach ($itemexp as $key => $ie) {
                        $itemexp[$key] = ucwords($ie);
                    }
                    $mn = str_replace('_', '', implode('_', $itemexp));
                    shell_exec('php cli db:rollback '.$mn);
                    $output->writeln('<info>Removing migration from file system</info>');
                    unlink($path);
                    $output->writeln('<info>Removing from migrations cache</info>');

                    $key = array_keys($file, $item);
                    array_splice($file, $key[0]);

                    $handle = fopen($migrationsTemplate, 'w');
                    foreach ($file as $f) {
                        fwrite($handle, $f);
                    }
                    fclose($handle);
                } else {
                    $output->writeln("<error>$migration does not exist!</error>");
                }
            }
        }
    }
}
