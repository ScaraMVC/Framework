<?php

namespace Scara\Console\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Model creation console command.
 */
class Model extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('db:model')
        ->setDescription('Create a new model')
        ->setDefinition(new InputDefinition([
            new InputArgument('name', InputArgument::REQUIRED),
        ]));
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $namel = strtolower($name).'s';

        $content = <<<EOF
<?php

namespace App\Models;

use Scara\Http\Model;

class $name extends Model
{
    // Table name here
    protected \$table = '$namel';

    // Mutable fields here
    protected \$fillable = [];
}

EOF;

        $cpath = app_path().'/models/';
        $filename = $name.'.php';
        $filepath = $cpath.$filename;

        if (!file_exists($filepath)) {
            $file = fopen($filepath, 'w');
            fwrite($file, $content);
            fclose($file);

            $output->writeln("<info>Model: $name created</info>");

            shell_exec('composer dump-autoload -o');
        } else {
            $output->writeln("<error>Model: $name already exists!");
        }
    }
}
