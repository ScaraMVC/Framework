<?php

namespace Scara\Console\Controller;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Controller creation console command.
 */
class CreateController extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('controller:make')
        ->setDescription('Create a new controller')
        ->setDefinition(new InputDefinition([
            new InputArgument('name', InputArgument::REQUIRED),
        ]));
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $content = <<<EOF
<?php

namespace App\Controllers;

use Scara\Http\Controller;
use Scara\Http\Request;

class $name extends Controller
{
    // Place methods here
}

EOF;

        $cpath = app_path().'/controllers/';
        $filename = $name.'.php';
        $filepath = $cpath.$filename;

        if (!file_exists($filepath)) {
            $file = fopen($filepath, 'w');
            fwrite($file, $content);
            fclose($file);

            $output->writeln("<info>Controller: $name created</info>");

            shell_exec('composer dump-autoload -o');
        } else {
            $output->writeln("<error>Controller: $name already exists!");
        }
    }
}
