<?php

namespace Scara\Console\Controller;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Controller deletion console command.
 */
class DeleteController extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('controller:drop')
        ->setDescription('Deletes a controller')
        ->setDefinition(new InputDefinition([
            new InputArgument('name', InputArgument::REQUIRED),
        ]));
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $cpath = app_path().'/controllers/';
        $filename = $name.'.php';
        $filepath = $cpath.$filename;

        if (file_exists($filepath)) {
            unlink($filepath);
            $output->writeln("<info>Controller: $name was deleted</info>");

            shell_exec('composer dump-autoload -o');
        } else {
            $output->writeln("<error>Controller: $name doesn't exists!");
        }
    }
}
