<?php

namespace Scara\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Publishes a plugin's configuration script.
 */
class ConfigPublish extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('config:publish')
        ->setDescription('Publish a configuration script for a plugin')
        ->setDefinition(new InputDefinition([
            new InputArgument('plugin', InputArgument::REQUIRED, 'Publishes a given plugin\'s config script'
                .' The plugin is denoted by vendor/package'),
        ]));
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $plugin = $input->getArgument('plugin');

        $pluginExp = explode('/', $plugin);

        $packageDir = config_path().'/'.$pluginExp[0];
        $configFile = $pluginExp[1].'.php';

        if (!file_exists($packageDir)) {
            $publish = true;
        } else {
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion(
                "$plugin already has a config script in place, are you sure you want to overwrite it? ",
                false,
                '/^yes|y/i'
            );

            $publish = $helper->ask($input, $output, $question);
        }

        if ($publish) {
            mkdir($packageDir);
            $pluginConfig = app_path().'/../vendor/'.$plugin.'/config.php';
            $publishConfig = $packageDir.'/'.$configFile;

            $file = fopen($pluginConfig, 'r');
            $content = fread($file, filesize($pluginConfig));
            fclose($file);

            $file = fopen($publishConfig, 'w');
            fwrite($file, $content);
            fclose($file);

            $output->writeln("<info>Configuration for $plugin published</info>");
        }
    }
}
