<?php

namespace Scara\Console;

use Scara\Hashing\OpenSslHasher;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Generates a new App key.
 */
class GenKey extends Command
{
    /**
     * Configure Symfony Command.
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName('genkey')
        ->setDescription('Generates a new 16 bit SSL key');
    }

    /**
     * Execute console command.
     *
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ssl = new OpenSslHasher();
        $key = $ssl->createiv();
        $envset = env_set('APP_TOKEN', $key);

        if ($envset) {
            $output->writeln('<info>App key generated: '.$key.'</>');
        } else {
            $output->writeln('<error>Error generating key!</>');
        }
    }
}
