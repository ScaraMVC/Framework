<?php

namespace Scara\Http\Groups;

use Scara\Auth\Authentication;

/**
 * Guest group.
 */
class Guest
{
    use Groupable;

    /**
     * Group init function.
     *
     * @param array $options
     *
     * @return void
     */
    public function init($options = [])
    {
        $auth = new Authentication();

        if ($auth->check()) {
            $url = (isset($options['guest_url']))
            ? $options['guest_url']
            : '/';

            $this->redirect = $url;
        }
    }
}
