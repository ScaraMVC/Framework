<?php

namespace Scara\Http\Groups;

use Scara\Auth\Authentication;

/**
 * Auth group.
 */
class Auth
{
    use Groupable;

    /**
     * Group init function.
     *
     * @param array $options
     *
     * @return void
     */
    public function init($options = [])
    {
        $auth = new Authentication();

        if (!$auth->check()) {
            $url = (isset($options['auth_login_url']))
            ? $options['auth_login_url']
            : '/auth/login';

            $this->redirect = $url;
        }
    }
}
