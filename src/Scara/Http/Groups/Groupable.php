<?php

namespace Scara\Http\Groups;

use Scara\Utils\UrlGenerator;

/**
 * Trait describing group classes.
 */
trait Groupable
{
    /**
     * URL for redirection if required.
     *
     * @var string
     */
    protected $redirect = '';

    /**
     * Finalize groups that implement the Groupable trait.
     *
     * @return void
     */
    public function finalize()
    {
        $gen = new UrlGenerator();
        if (!empty($this->redirect)) {
            $gen->redirect($this->redirect);
        }
    }
}
