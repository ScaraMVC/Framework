<?php

namespace Scara\Http;

use Scara\Config\Configuration;
use Scara\Database\Database;
use Scara\Pagination\Pagination;

/**
 * Database Model master class.
 */
class Model
{
    /**
     * Fillable options.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The model's table.
     *
     * @var string
     */
    protected $table = '';

    /**
     * Config instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Database Instance.
     *
     * @var \Scara\Database\Database
     */
    private $_database;

    /**
     * Model instance.
     *
     * @var mixed
     */
    protected $_model;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration();
        $this->_config = $c->from('database');
        $this->_database = new Database();
    }

    /**
     * Here in case the user wants a quick and dirty model getter.
     *
     * @return mixed
     */
    public static function init()
    {
        $class = get_called_class();
        $model = new $class();

        return $model;
    }

    /**
     * Creates a new table row.
     *
     * @return bool
     */
    public function create($options = [])
    {
        $capsule = $this->_database->getCapsule();
        if (empty($options) || !is_array($options)) {
            throw new \Exception('Create options must not be empty and must be an instance of an array!');
        }

        // filter out options not given in fillable array
        $this->filter($options);

        $options['created_at'] = date('Y-m-d h:i:s');
        $options['updated_at'] = $options['created_at'];

        if ($capsule->table($this->table)->insert($options)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Deletes a record from the database.
     *
     * @param int $id
     *
     * @return int
     */
    public function delete($id)
    {
        $capsule = $this->_database->getCapsule();

        return $capsule->table($this->table)->delete($id);
    }

    /**
     * Finds an item by it's id.
     *
     * @param int $id
     *
     * @return stdClass
     */
    public function find($id)
    {
        $capsule = $this->_database->getCapsule();

        $this->_model = $capsule->table($this->table)->where('id', '=', $id);

        return $this->_model->first();
    }

    /**
     * Searches for something in the database.
     *
     * @param string $column   - The column to search
     * @param string $operator - The operator for searching with
     * @param string $value    - The value you're looking for
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function where($column, $operator, $value)
    {
        $capsule = $this->_database->getCapsule();

        if (empty($column)) {
            throw new \Exception('You must provide the column to look up!');
        }
        if (empty($operator)) {
            throw new \Exception('You must provide an operator!');
        }
        if (empty($value)) {
            throw new \Exception('You must provide the value!');
        }

        $this->_model = $capsule->table($this->table)->where($column, $operator, $value);

        return $this->_model;
    }

    /**
     * Gets the number of results.
     *
     * @return int
     */
    public function count()
    {
        if (is_array($this->_model)) {
            return count($this->_model);
        } else {
            return $this->_model->count();
        }
    }

    /**
     * Gets all results from the database.
     *
     * @param string $order - The order of the results
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function all($order = 'ASC')
    {
        $capsule = $this->_database->getCapsule();

        $this->_model = $capsule->table($this->table)->orderBy('id', $order)->get();

        return $this->_model;
    }

    /**
     * Selects from the database table via custom SQL command.
     *
     * @param \Illuminate\Database\Query\Builder $capsule - Capsule instance
     * @param int                                $limit   - Selection limit
     * @param int                                $offset  - Selection offset
     * @param array                              $columns - Columns to select from
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function select($capsule = null, $limit = null, $offset = null, $columns = [])
    {
        if (is_null($capsule)) {
            $capsule = $this->_database->getCapsule()->table($this->table);
        }

        if (empty($columns)) {
            $columns = '*';
        } else {
            $cstring = '';
            foreach ($columns as $column) {
                $cstring .= "{$column}, ";
            }
            $columns = rtrim($cstring, ', ');
        }

        $sql = $capsule->select("{$columns}");
        if (!is_null($limit)) {
            $sql->limit($limit);
        }
        if (!is_null($offset)) {
            $sql->offset($offset);
        }

        return $sql;
    }

    /**
     * Does like all, but creates pagination for results.
     *
     * @param int $limit - The number of results to limit by
     *
     * @return \Scara\Pagination\Pagination
     */
    public function paginate($limit)
    {
        $capsule = $this->_database->getCapsule();
        $this->_model = $capsule->table($this->table);
        $paginator = new Pagination();
        $paginator->setLimit($limit)->setModel($this, $this->_model);

        return $paginator->paginate();
    }

    /**
     * Saves changes to a model.
     *
     * @param mixed $options
     *
     * @return bool
     */
    public function save($options)
    {
        $gcc = get_called_class();
        $class = $gcc::init();
        $cap = $this->_database->getCapsule();
        $params = (array) $options;
        $this->filter($params);

        return $cap->table($this->table)->update($params);
    }

    /**
     * Gets a child model for model relationships.
     *
     * @param string $child
     *
     * @return \Scara\Http\Model
     */
    protected function getChild($child)
    {
        return $child::init();
    }

    /**
     * Filters out options that are not included in a model's fillable array.
     *
     * @param array &$options - Options array reference
     *
     * @return void
     */
    private function filter(&$options)
    {
        foreach ($options as $key => $value) {
            if (!in_array($key, $this->fillable)) {
                unset($options[$key]);
            }
        }
    }
}
