<?php

namespace Scara\Http;

/**
 * HTTP Request object.
 */
class Request
{
    /**
     * The request object.
     *
     * @var \Scara\Http\Request
     */
    private $_request;

    /**
     * Class Constructor // Casts request array to a new Request object if given.
     *
     * @param array $request
     */
    public function __construct($request = [])
    {
        $this->cast($request);
    }

    /**
     * Gets all request objects as an array.
     *
     * @return array
     */
    public function all()
    {
        $request = $this;
        unset($request->_request);
        unset($request->error);

        return (array) $request;
    }

    /**
     * Casts the request array into new Request object.
     *
     * @param array $request - The request object
     */
    public function cast($request)
    {
        if (is_array($request) || is_object($request)) {
            foreach ($request as $key => $value) {
                if (is_array($value)) {
                    $this->cast($value);
                } else {
                    $this->$key = $value;
                }
            }
        }
    }

    /**
     * Returns the request as a new Request object.
     *
     * @return \Scara\Http\Request
     */
    public function getObject()
    {
        return new self($this->_request);
    }

    /**
     * Sets the request.
     *
     * @param array $request - The request object
     *
     * @throws \Exception
     *
     * @return Request
     */
    public function setRequest($request = [])
    {
        if (!empty($request)) {
            if (is_array($request)) {
                foreach ((array) $request as $item) {
                    foreach ($item as $key => $value) {
                        if (empty($value)) {
                            $request['empty'] = 'true';
                        } else {
                            $request['empty'] = 'false';
                        }
                    }
                    break;
                }
                $this->_request = $request;

                return $this;
            } else {
                throw new \Exception('Initial request object must be an instance of an array!');
            }
        } else {
            throw new \Exception('An empty request object was given!');
        }
    }
}
