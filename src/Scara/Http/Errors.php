<?php

namespace Scara\Http;

/**
 * Framework default HTTP errors controller.
 */
class Errors extends Controller
{
    /**
     * Default 404 error.
     *
     * @return void
     */
    public function error404()
    {
        die('The page you requested does not exist!');
    }

    /**
     * Default 400 error.
     *
     * @return void
     */
    public function error400()
    {
        die('Request sent was bad!');
    }
}
