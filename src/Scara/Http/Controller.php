<?php

namespace Scara\Http;

use Input;
use Scara\Routing\RouteLoader;
use Scara\Routing\Router;
use Scara\Session\SessionInitializer;
use Scara\Utils\UrlGenerator;
use Scara\Validation\Errors;

/**
 * Loads controller route and executes requested controller/method.
 */
class Controller
{
    /**
     * The view to be loaded.
     *
     * @var \Scara\Http\View
     */
    private $_view;

    /**
     * The requested route for loading controllers.
     *
     * @var array
     */
    private $_route;

    /**
     * URL Generator.
     *
     * @var \Scara\Utils\UrlGenerator
     */
    private $_generator;

    /**
     * Validation errors instance.
     *
     * @var \Scara\Validation\Errors
     */
    private $_errors;

    /**
     * Form input instance.
     *
     * @var array
     */
    private $_input;

    /**
     * Session instance.
     *
     * @var mixed
     */
    private $_session;

    /**
     * Class Constructor // Creates new View instance.
     *
     * @param array $route - Route to load if given
     */
    public function __construct($route = [])
    {
        $this->_view = new View();
        $this->_generator = new UrlGenerator();
        $this->_errors = new Errors();
        $this->_input = new Errors(); // We'll use the same validation errors class for input too
        $this->_session = SessionInitializer::init();
    }

    /**
     * Adds data to the view class that will pass on to
     * the view.
     *
     * @param string $key   - Data's key
     * @param mixed  $value - $key's value
     *
     * @return \Scara\Http\Controller
     */
    public function add($key, $value = null)
    {
        $this->_view->with($key, $value);

        return $this;
    }

    /**
     * Another way of using Controller->add.
     *
     * @param string $key   - Data's key
     * @param mixed  $value - $key's value
     *
     * @return \Scara\Http\Controller
     */
    public function with($key, $value = null)
    {
        $this->add($key, $value);

        return $this;
    }

    /**
     * Adds a flash message to be displayed once on rendering a view.
     *
     * @param string $key   - Flash data's key
     * @param string $value - $key's value
     *
     * @return \Scara\Http\Controller
     */
    public function flash($key, $value = '')
    {
        $this->_session->flash($key, $value);

        return $this;
    }

    /**
     * Flash errors grabbed from a request.
     *
     * @param \Scara\Validation\Validator $validator - Validation object
     *
     * @return \Scara\Http\Controller
     */
    public function withErrors($validator)
    {
        foreach ($validator->errors() as $key => $value) {
            if (count($value) > 1) {
                for ($i = 0; $i < count($value); $i++) {
                    $data[$key][$i] = $value[$i];
                }
            } else {
                $data[$key] = $value[0];
            }
            $this->_errors->add((object) $data);
        }

        $this->_session->flash('errors', $this->_errors->all());

        return $this;
    }

    /**
     * Renders a view.
     *
     * @param string $path - View's path
     *
     * @return \Scara\Http\Controller
     */
    public function render($path)
    {
        if ($this->_session->has('input')) {
            $this->_input->add($this->_session->flash('input'));
        }

        if ($this->_session->has('errors')) {
            $this->_errors->add($this->_session->flash('errors'));
        }

        $this->add('errors', $this->_errors);
        $this->add('input', $this->_input);

        $this->_view->render($path);

        return $this;
    }

    /**
     * Renders a view with data to pass on to the view.
     *
     * @param string $path - View's path
     * @param array  $data - The data to render with
     *
     * @return \Scara\Http\Controller
     */
    public function renderWithData($path, $data = [])
    {
        foreach ($data as $key => $value) {
            $this->add($key, $value);
        }

        $this->render($path);

        return $this;
    }

    /**
     * Redirect to a URL.
     *
     * @param string $url - Redirection's URL
     *
     * @return void
     */
    public function redirect($url)
    {
        $this->_generator->redirect($url);
    }

    /**
     * Redirects to a URL with data passed in as a temp flash session.
     *
     * @param string $url  - Redirection's URL
     * @param mixed  $data - Data to redirect with
     *
     * @return void
     */
    public function redirectWith($url, $data)
    {
        $this->_session->flash('redirect_data', $data);
        $this->redirect($url);
    }

    /**
     * Flashes form input if redirecting.
     *
     * @return \Scara\Http\Controller
     */
    public function withInput()
    {
        $this->_session->flash('input', Input::all());

        return $this;
    }

    /**
     * Loads in a group for given controller method.
     *
     * @param string $name - The group name
     * @param array  $data - Data to pass to group
     *
     * @throws \Exception
     *
     * @return \Scara\Http\Controller
     */
    public function group($name, $data = [])
    {
        $app = 'App\Groups\\'.ucwords($name);
        $core = 'Scara\Http\Groups\\'.ucwords($name);

        if (class_exists($app)) {
            $class = new $app();
        } else {
            if (class_exists($core)) {
                $class = new $core();
            } else {
                throw new \Exception("$name group could not be found in the application or in Scara's core");
            }
        }

        if (method_exists($class, 'init')) {
            if (!empty($data)) {
                $class->init($data);
            } else {
                $class->init();
            }

            if (in_array('Scara\Http\Groups\Groupable', class_uses($class))) {
                // class uses Groupable trait. call finalize
                $class->finalize();
            } elseif (method_exists($class, 'finalize')) {
                $class->finalize(); // not using Groupable trait, but still has a finalize method defined
            }
        } else {
            throw new \Exception("$name group does not define the required init method!");
        }

        return $this;
    }

    /**
     * Loads the controller with the requested route.
     *
     * @param \Scara\Routing\Router $router   - The router
     * @param string                $basepath - Application's base path
     *
     * @return void
     */
    public function load(Router $router, $basepath)
    {
        $loader = new RouteLoader();
        $loader->registerRouter($router);
        $this->_route = $loader->getRequestedRoute($basepath);

        if (isset($this->_route['method']) && $this->_route['method'] == $_SERVER['REQUEST_METHOD']) {
            $controller = 'App\Controllers\\'.$this->_route['controller'];

            if (!class_exists($controller)) {
                throw new \Exception("Class [{$this->_route['controller']}] does not exist!");
            }

            $class = new $controller($this->_route);
            $method = $this->_route['action'];

            if (isset($this->_route['query'])) {
                $request = new Request();

                if (method_exists($class, $method)) {
                    $class->$method($request->setRequest($this->_route['query'])->getObject());
                } else {
                    throw new \Exception("Method [$method] is not defined in $controller!");
                }
            } else {
                if (method_exists($class, $method)) {
                    $class->$method();
                } else {
                    throw new \Exception("Method [$method] is not defined in $controller!");
                }
            }
        } else {
            $this->loadErrorPage('404');
        }
    }

    /**
     * Loads a requested error page.
     *
     * @param string $errorType - Error type to render for
     *
     * @return void
     */
    public function loadErrorPage($errorType)
    {
        switch ($errorType) {
            case 'notfound':
            $code = '404';
            break;
            case 'badrequest':
            $code = '400';
            break;
            default:
            $code = $errorType;
            break;
        }
        require_once \Config::from('app')->get('errors');
        if (isset($errors)) {
            foreach ($errors as $error) {
                $aexp = explode('@', $error);
                $c = 'App\\Controllers\\'.$aexp[0];
                if (class_exists($c)) {
                    $method = 'error'.$code;
                    if (method_exists($c, $method)) {
                        $class = new $c();
                        if ($method == 'error'.$code) {
                            $class->$method();
                        }
                        exit; // quick hack for now
                    } else {
                        $class = new Errors();
                        $method = 'error'.$code;
                        $class->$method();
                    }
                } else {
                    $class = new Errors();
                    $method = 'error'.$code;
                    $class->$method();
                }
            }
        } else {
            $class = new Errors();
            $method = 'error'.$code;
            $class->$method();
        }
    }

    /**
     * Gets the current loaded route.
     *
     * @deprecated
     *
     * @return array
     */
    public function getRoute()
    {
        return $this->_route;
    }
}
