<?php

namespace Scara\Validation;

use Scara\Support\ServiceProvider;

/**
 * Validation service provider.
 */
class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Registers the validation service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('validator', function () {
            return new Validator();
        });
    }
}
