<?php

namespace Scara\Validation;

use Scara\Config\Configuration;

/**
 * Form Validation class.
 */
class Validator
{
    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * The language translation.
     *
     * @var array
     */
    private $_lang;

    /**
     * Validation Parser instance.
     *
     * @var \Scara\Validation\ValidationParser
     */
    private $_parser;

    /**
     * Errors array.
     *
     * @var array
     */
    private $_errors = [];

    /**
     * Input object.
     *
     * @var array
     */
    private $_input;

    /**
     * Class Constructor // Gets config and language pack.
     */
    public function __construct()
    {
        $config = new Configuration();
        $this->_config = $config->from('app');

        $this->getLang();
    }

    /**
     * Gets the language pack.
     *
     * @return void
     */
    private function getLang()
    {
        $path = $this->_config->get('langpath')
        .$this->_config->get('lang');

        if (file_exists($path.'/validation.php')) {
            $this->_lang = require_once $path.'/validation.php';
        } else {
            $this->_lang = require_once $this->_config->get('langpath').'/en_US/validation.php';
        }

        // Handle any custom language validation values
        // by parser
        $this->_parser = new ValidationParser($this->_lang, $this->_config->get('validation'));
    }

    /**
     * Creates validation.
     *
     * @param array $input - Form input array
     * @param array $rules - Validation rules
     *
     * @return \Scara\Validation\Validator
     */
    public function make($input, $rules = [])
    {
        $this->_input = $input;
        foreach ($rules as $key => $value) {
            $this->validateInput($input, $rules, $key);
        }

        return $this;
    }

    /**
     * Checks to see if the validation passed.
     *
     * @return bool
     */
    public function isValid()
    {
        $valid = true;
        foreach ($this->_errors as $input => $message) {
            foreach ($message as $item) {
                if (!empty($item)) {
                    $valid = false;
                }
            }
        }

        foreach ($this->_input as $key => $value) {
            \Session::delete('input.'.$key);
        }

        return $valid;
    }

    /**
     * Returns the errors array.
     *
     * @return array
     */
    public function errors()
    {
        return $this->_errors;
    }

    /**
     * Validates the given input.
     *
     * @param array  $input - Form input array
     * @param array  $rules - Validation rules
     * @param string $key   - Validation option
     *
     * @return void
     */
    private function validateInput($input, $rules, $key)
    {
        if (isset($input[$key]) && isset($rules[$key])) {
            $ruleExp = explode('|', $rules[$key]);

            foreach ($ruleExp as $rule) {
                $this->parseRule($rule, $input, $key);
            }
        }
    }

    /**
     * Parses a given rule with the given input.
     *
     * @param string $rule  - The rule being parsed
     * @param array  $input - Form input array
     * @param string $key   - Validation option
     */
    private function parseRule($rule, $input, $key)
    {
        $ruleExp = explode(':', $rule);
        if (count($ruleExp) > 1) {
            switch ($ruleExp[0]) {
                case 'match':
                $this->_errors[$key][] = $this->_parser->match($input, $key, $ruleExp[1], $this->_lang['match']);
                break;
                case 'min':
                $this->_errors[$key][] = $this->_parser->min($input, $key, $ruleExp[1], $this->_lang['min']);
                break;
                case 'max':
                $this->_errors[$key][] = $this->_parser->max($input, $key, $ruleExp[1], $this->_lang['max']);
                break;
                case 'from':
                $this->_errors[$key][] = $this->_parser->from($input, $key, $rule, $this->_lang['from']);
                break;
            }
        } else {
            switch ($rule) {
                case 'required':
                $this->_errors[$key][] = $this->_parser->required($input, $key, $this->_lang['required']);
                break;
                case 'email':
                $this->_errors[$key][] = $this->_parser->email($input, $key, $this->_lang['email']);
                break;
                case 'url':
                $this->_errors[$key][] = $this->_parser->url($input, $key, $this->_lang['url']);
                break;
            }
        }
    }
}
