<?php

namespace Scara\Validation;

/**
 * Handles grabbing validation errors for rendering in views.
 */
class Errors
{
    /**
     * Array of errors (converted into stdObject later).
     *
     * @var array
     */
    private $_errors = [];

    /**
     * Looks to see if the there's a specific error.
     *
     * @param string $key - The error we're looking for
     *
     * @return bool
     */
    public function has($key)
    {
        return isset($this->_errors[$key]);
    }

    /**
     * Gets a specific error.
     *
     * @param string $key - The error we're looking for
     *
     * @return string - The error message
     */
    public function get($key)
    {
        $res = $this->_errors[$key];
        if (!is_array($res)) {
            return [$res];
        }

        return $res;
    }

    /**
     * Gets the first message for a specific error.
     *
     * @param string $key - The error we're looking for
     *
     * @return string - The error message
     */
    public function first($key)
    {
        if ($this->has($key)) {
            $err = $this->_errors[$key];
            if (is_array($err)) {
                for ($i = 0; $i < count($err); $i++) {
                    if (!empty($err[$i])) {
                        return $err[$i];
                    }
                }
            } else {
                return $err;
            }
        }

        return ''; // return empty string since no key found to prevent errors
    }

    /**
     * Adds all error data. Also clears out any null values.
     *
     * @param array $data - The error data to load into
     */
    public function add($data)
    {
        $ea = (array) $data;
        foreach ($ea as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $k => $v) {
                    if (is_null($v)) {
                        unset($value[$k]);
                    }
                }
                $value = array_values($value);
                $ea[$key] = $value;
            }
        }

        $this->_errors = $ea;
    }

    /**
     * Returns an object representation of all errors.
     *
     * @return stdObject
     */
    public function all()
    {
        return (object) $this->_errors;
    }

    /**
     * Returns an error object as string.
     *
     * @return string
     */
    public function __get($property)
    {
        return $this->first($property);
    }
}
