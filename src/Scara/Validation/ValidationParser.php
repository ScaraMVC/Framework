<?php

namespace Scara\Validation;

use Scara\Database\Database;
use Scara\Debug\ExceptionHandler;

/**
 * Parses validation config and displays form validation errors.
 */
class ValidationParser
{
    /**
     * Replaces default lang string with custom one.
     *
     * @var array
     */
    private $_replaceWithCustom = [];

    /**
     * The custom lang string to replace with.
     *
     * @var array
     */
    private $_custom = [];

    /**
     * Class Constructor // Handles setting custom validation strings.
     *
     * @param array $lang   - The language used for validation
     * @param array $custom - Custom validations set in config/app.php
     */
    public function __construct($lang, $custom)
    {
        // ExceptionHandler::handleExceptions();

        foreach ($lang as $key => $value) {
            if (isset($custom[$key])) {
                $this->_replaceWithCustom[$key] = true;
            } else {
                $this->_replaceWithCustom[$key] = false;
            }
        }

        $this->_custom = $custom;
    }

    /**
     * Parses a required validation field.
     *
     * @param array  $input - Input to be validated
     * @param string $key   - Validation key
     * @param string $lang  - Language string
     *
     * @return string
     */
    public function required($input, $key, $lang)
    {
        $lang = $this->custom('required', $lang);
        if (empty($input[$key])) {
            return str_replace(':field', $key, $lang);
        }
    }

    /**
     * Parses a match validation field.
     *
     * @param array  $input - Input to be validated
     * @param string $key   - Validation key
     * @param string $match - The option looking to be matched up with $key
     * @param string $lang  - Language string
     *
     * @return string
     */
    public function match($input, $key, $match, $lang)
    {
        $lang = $this->custom('match', $lang);
        if ($input[$key] !== $input[$match]) {
            return preg_replace_callback('/(\:field)(.*)(\:match)/i',
            function ($m) use ($key, $match) {
                return $key.$m[2].$match;
            }, $lang);
        }
    }

    /**
     * Parses an email validation field.
     *
     * @param array  $input - Input to be validated
     * @param string $key   - Validation key
     * @param string $lang  - Language string
     *
     * @return string
     */
    public function email($input, $key, $lang)
    {
        $lang = $this->custom('email', $lang);
        if (filter_var($input[$key], FILTER_VALIDATE_EMAIL) === false) {
            return str_replace(':field', $key, $lang);
        }
    }

    /**
     * Parses URL validation field.
     *
     * @param array  $input - Input to be validated
     * @param string $key   - Validation key
     * @param string $lang  - Language string
     *
     * @return string
     */
    public function url($input, $key, $lang)
    {
        $lang = $this->custom('url', $lang);
        if (filter_var($input[$key], FILTER_VALIDATE_URL) === false) {
            return str_replace(':field', $key, $lang);
        }
    }

    /**
     * Parses a set minimum number of characters.
     *
     * @param array  $input - Language string
     * @param string $key   - Validation Key
     * @param int    $min   - The set minimum
     * @param string $lang  - Language string
     *
     * @return mixed
     */
    public function min($input, $key, $min, $lang)
    {
        $lang = $this->custom('min', $lang);
        if (strlen($input[$key]) < $min) {
            return preg_replace_callback('/(:field)(.*)(\:num)/i',
            function ($m) use ($key, $min) {
                return $key.$m[2].$min;
            }, $lang);
        }
    }

    /**
     * Parses a set maximum number of characters.
     *
     * @param $input - Language string
     * @param string $key  - Validation Key
     * @param int    $max  - The set maximum
     * @param string $lang - Language string
     *
     * @return mixed
     */
    public function max($input, $key, $max, $lang)
    {
        $lang = $this->custom('max', $lang);
        if (strlen($input[$key]) > $max) {
            return preg_replace_callback('/(:field)(.*)(\:num)/i',
            function ($m) use ($key, $max) {
                return $key.$m[2].$max;
            }, $lang);
        }
    }

    /**
     * Checks if a given field exists in given database table.
     *
     * @param $input - Language string
     * @param string $key  - Validation Key
     * @param array  $rule - The entire rule (Needed for multi-parsing)
     * @param string $lang - Language string
     *
     * @return mixed
     */
    public function from($input, $key, $rule, $lang)
    {
        $lang = $this->custom('from', $lang);
        $db = new Database();
        $cap = $db->getCapsule();

        $exp = explode(':', $rule);
        $table = $cap->table($exp[1]);

        if ($table->where($exp[2], '=', $input[$key])->count() == 0) {
            $tn = $exp[1];

            return preg_replace_callback('/(:field)(.*)(\:from)/i',
            function ($m) use ($input, $key, $tn) {
                return $input[$key].$m[2].$tn;
            }, $lang);
        }
    }

    /**
     * Handles replacing language string with any given custom ones defined in config/app.php.
     *
     * @param string $key  - Language key
     * @param string $lang - Language string
     *
     * @return mixed
     */
    protected function custom($key, $lang)
    {
        return ($this->_replaceWithCustom[$key]) ? $this->_custom[$key] : $lang;
    }
}
