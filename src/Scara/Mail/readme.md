# Scara Mailer
This module is the mailer module built for Scara. However, it's built to be `modular` so it'll work just fine outside of the framework. This readme will explain how to use the module outside of Scara. Please refer to Scara's documentation to learn how to use it from the framework.

## Installing
To install run `composer require scaramvc/mail` to get the latest version, and be sure to include Composer's autoloader.

## Using
To use, include the autoloader, and add a `use` statement to use the class. When you have that, you'll create a new class instance with host info (hostname/port) and extras like what encryption (tls/ssl etc.) and if you want to use SMTP. Afterwords, create the mail, add a body, etc. and send.

```php
<?php

require_once __DIR__.'/vendor/autoload.php';

use Scara\Mail\Mailer;

$mail = new Mailer('smtp.domail.com', 587); // New Mailer instance
$mail->setAuthentication('user@domail.com', 'supersecretpassword'); // Set SMTP auth user

// Create the mail with sender/recipient (optional subject)
$mail->create([
    'to' => [
        'user2@domail.com',
        'John Doe'
    ],
    'from' => 'no-reply@domail.com',
    'subject' => 'Super Awesome Email',
]);

// Add a body to the email (html supported)
$mail->addBody('This is a cool email. Oh, and plain text body is also taken from HTML');

// send
if ($mail->send()) {
    echo 'Mail Sent!';
} else {
    echo $mail->getError(); // Display the error if it failed.
}
```
