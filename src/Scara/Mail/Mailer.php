<?php

namespace Scara\Mail;

use PHPMailer;

/**
 * Mailer class. Used PHPMailer.
 */
class Mailer
{
    /**
     * PHPMailer instance.
     *
     * @var \PHPMailer
     */
    private $_mailer;

    /**
     * The recipient of the email.
     *
     * @var string
     */
    public $to;

    /**
     * The sender of the email.
     *
     * @var string
     */
    public $from;

    /**
     * The email subject.
     *
     * @var string
     */
    public $subject;

    /**
     * The body. Can be HTML if html is set to true.
     *
     * @var string
     */
    public $body;

    /**
     * Sets whether the email is an HTML email or not.
     *
     * @var bool
     */
    public $html = true;

    /**
     * Class constructor. Builds initial PHPMailer object.
     *
     * @param string $hostname
     * @param int    $port
     * @param bool   $use_smtp
     * @param string $encryption
     *
     * @return \Scara\Mail\Mailer
     */
    public function __construct($hostname, $port, $use_smtp = true, $encryption = 'ssl')
    {
        $this->_mailer = new PHPMailer();
        $this->_mailer->Host = $hostname;
        $this->_mailer->Port = $port;

        if ($use_smtp) {
            $this->_mailer->isSMTP();
            $this->_mailer->SMTPAuth = true;
            $this->_mailer->SMTPSecure = $encryption;
        }

        return $this;
    }

    /**
     * Sets the authentication used to send the mail.
     *
     * @param string $username
     * @param string $password
     *
     * @return \Scara\Mail\Mailer
     */
    public function setAuthentication($username, $password)
    {
        $this->_mailer->Username = $username;
        $this->_mailer->Password = $password;

        return $this;
    }

    /**
     * Creates the initial message. Takes in to, from and subject.
     *
     * @param array $options
     *
     * @return \Scara\Mail\Mailer
     */
    public function create($options = [])
    {
        if (isset($options['to'])) {
            $name = '';

            if (is_array($options['to'])) {
                if (isset($options['to'][1])) {
                    $name = $options['to'][1];
                }

                $this->setTo($options['to'][0], $name);
            } else {
                $this->setTo($options['to']);
            }
        }

        if (isset($options['from'])) {
            $name = '';

            if (is_array($options['from'])) {
                if (isset($options['from'][1])) {
                    $name = $options['from'][1];
                }

                $this->setFrom($options['from'][0], $name);
            } else {
                $this->setFrom($options['from']);
            }
        }

        if (isset($options['subject'])) {
            $this->addSubject($options['subject']);
        }

        return $this;
    }

    /**
     * Sets the Recipient of the email.
     *
     * @param string $email
     * @param string $name
     *
     * @return \Scara\Mail\Mailer
     */
    public function setTo($email, $name = '')
    {
        if (empty($email)) {
            throw new MailerException('email is required for setTo!');
        }

        if (empty($name)) {
            $name = substr($email, 0, strrpos($email, '@'));
        }

        if (is_null($this->to)) {
            $this->to = [$email, $name];
        } else {
            throw new MailerException('A master recipient has already been set!');
        }

        return $this;
    }

    /**
     * Sets the Sender of the email.
     *
     * @param string $email
     * @param string $name
     *
     * @return \Scara\Mail\Mailer
     */
    public function setFrom($email, $name = '')
    {
        if (empty($email)) {
            throw new MailerException('email is required for setFrom!');
        }

        if (empty($name)) {
            $name = substr($email, 0, strrpos($email, '@'));
        }

        if (is_null($this->from)) {
            $this->from = [$email, $name];
        } else {
            throw new MailerException('Sender has already been set!');
        }

        return $this;
    }

    /**
     * Adds a CC to the email.
     *
     * @param string $email
     * @param string $name
     *
     * @return \Scara\Mail\Mailer
     */
    public function cc($email, $name = '')
    {
        if (empty($email)) {
            throw new MailerException('email is required for cc!');
        }

        $this->_mailer->addCC($email, $name);

        return $this;
    }

    /**
     * Adds a VCC to the email.
     *
     * @param string $email
     * @param string $name
     *
     * @return \Scara\Mail\Mailer
     */
    public function bcc($email, $name = '')
    {
        if (empty($email)) {
            throw new MailerException('email is required for bcc!');
        }

        $this->_mailer->addBCC($email, $name);

        return $this;
    }

    /**
     * Adds a recipient to the email.
     *
     * @param string $email
     * @param string $name
     *
     * @return \Scara\Mail\Mailer
     */
    public function addAddress($email, $name = '')
    {
        if (empty($email)) {
            throw new MailerException('email is required for addAddress!');
        }

        $this->_mailer->addAddress($email, $name);

        return $this;
    }

    /**
     * Attaches a file to the email.
     *
     * @param string $file
     *
     * @return \Scara\Mail\Mailer
     */
    public function attach($file)
    {
        if (empty($file)) {
            throw new MailerException('a file is required to use attach!');
        } elseif (!file_exists($file)) {
            throw new MailerException("$file does not exist!");
        }

        $this->_mailer->addAttachment($file);

        return $this;
    }

    /**
     * Adds a subject to the mail.
     *
     * @param string $subject
     *
     * @return \Scara\Mail\Mailer
     */
    public function addSubject($subject)
    {
        if (empty($subject)) {
            throw new MailerException('subject is required!');
        }

        $this->subject = $subject;

        return $this;
    }

    /**
     * Adds the body to the email.
     *
     * @param string $body
     *
     * @return \Scara\Mail\Mailer
     */
    public function addBody($body)
    {
        if (empty($body)) {
            throw new MailerException('body is required!');
        }

        $this->body = $body;

        return $this;
    }

    /**
     * Sends the constructed email.
     *
     * @return bool
     */
    public function send()
    {
        $this->_mailer->addAddress($this->to[0], $this->to[1]);
        $this->_mailer->setFrom($this->from[0], $this->from[1]);
        $this->_mailer->isHtml($this->html);
        $this->_mailer->Subject = $this->subject;
        $this->_mailer->Body = $this->body;
        $this->_mailer->AltBody = strip_tags($this->body);

        return $this->_mailer->send();
    }

    /**
     * Returns the PHPMailer error if one is thrown.
     *
     * @return string
     */
    public function getError()
    {
        return $this->_mailer->ErrorInfo;
    }
}
