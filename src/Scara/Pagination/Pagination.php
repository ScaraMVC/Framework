<?php

namespace Scara\Pagination;

use Scara\Config\Configuration;
use Scara\Http\Model;
use Scara\Http\View;

/**
 * Pagination class for creating pagination results.
 */
class Pagination
{
    /**
     * Model instance.
     *
     * @var \Scara\Http\Model
     */
    private $_model;

    /**
     * Capsule instance.
     *
     * @var \Illuminate\Database\Query\Builder
     */
    private $_capsule;

    /**
     * Pagination limit.
     *
     * @var int
     */
    private $_limit;

    /**
     * Pagination offset.
     *
     * @var int
     */
    private $_offset;

    /**
     * Query results.
     *
     * @var mixed
     */
    private $_results;

    /**
     * Current page.
     *
     * @var int
     */
    private $_page;

    /**
     * Total number of pages.
     *
     * @var int
     */
    private $_totalPages;

    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_page = (isset($_GET['page'])) ? $_GET['page'] : 1;

        $c = new Configuration();
        $this->_config = $c->from('app');
    }

    /**
     * Sets the model instance for the Pagination class.
     *
     * @param \Scara\Http\Model                  $model
     * @param \Illuminate\Database\Query\Builder $capsule
     *
     * @return \Scara\Pagination\Pagination
     */
    public function setModel(Model $model, $capsule)
    {
        $this->_model = $model;
        $this->_capsule = $capsule;

        return $this;
    }

    /**
     * Sets the pagination limit.
     *
     * @param int $limit
     *
     * @return \Scara\Pagination\Pagination
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;

        return $this;
    }

    /**
     * Handles the pagination.
     *
     * @return \Scara\Pagination\Pagination
     */
    public function paginate()
    {
        $count = count($this->_model->all());
        $this->_totalPages = ceil($count / $this->_limit);
        $page_at_offset = floor($this->_page + 1 / $this->_limit);
        $this->_offset = ($page_at_offset - 1) * $this->_limit;

        $this->_results = $this->_model->select($this->_capsule, $this->_limit, $this->_offset);

        return $this;
    }

    /**
     * Sets the model instance for the Pagination class.
     *
     * @return int
     */
    public function count()
    {
        return $this->_model->count();
    }

    /**
     * Sets the model instance for the Pagination class.
     *
     * @return mixed
     */
    public function get()
    {
        return $this->_results->get();
    }

    /**
     * Renders pagination links.
     *
     * @param string $path  - The view path
     * @param string $views - Where the pagination view is stored
     *
     * @return mixed
     */
    public function render($path = 'basic', $views = null)
    {
        $view = new View();
        if (is_null($views)) {
            $views = __DIR__.'/views/';
        }

        $view->createBlade($views, $this->_config->get('cache'));

        return $view->with('pagination', [
            'currentPage' => $this->_page,
            'totalPages'  => $this->_totalPages,
            ])->render($path);
    }
}
