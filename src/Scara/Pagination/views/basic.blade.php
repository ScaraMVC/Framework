@for($i = 1; $i <= $pagination['totalPages']; ++$i)

    @if($i == $pagination['currentPage'])
        {{ $i }}
    @else
        <a href="?page={{ $i }}">{{ $i }}</a>
    @endif

@endfor
