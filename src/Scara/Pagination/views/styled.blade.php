<style type="text/css">
.pagination_block {
    margin-top: 5px;
    font-size: 14px;
}
.pagination_block .pagination_row_item {
    float: left;
    margin-right: 5px;
}
.pagination_block .pagination_row_item > a.pagination_cell {
    border: 1px solid #008cba;
    padding: 7px 10px;
    text-align: center;
    line-height: 14px;
    display: block;
    color: #FFF;
    background-color: #008cba;
}
.pagination_block .pagination_row_item > span.pagination_cell {
    text-align: center;
    padding: 7px 10px;
    line-height: 14px;
    display: block;
}
.pagination_block .pagination_row_item > a.pagination_cell:hover {
    background-color: #006687;
    text-decoration: none;
}
</style>
<script type="text/javascript">
$(function()
{
    var inactiveLink = $("a.inactive");

    if(inactiveLink.hasClass("pagination_cell"))
    {
        var content = inactiveLink.html();
        var item = inactiveLink.parent();
        inactiveLink.remove();
        item.append("<span class=\"pagination_cell inactive\">" + content + "</span>");
    }
});
</script>
<div class="pagination_block">
    <div class="pagination_row_item">
        @if($pagination['currentPage'] == 1)
            <span class="pagination_cell inactive">&laquo; First</span>
        @else
            <a href="?page=1" class="pagination_cell">&laquo; First</a>
        @endif
    </div>
    @for($i = 1; $i <= $pagination['totalPages']; $i++)
        <div class="pagination_row_item">
            @if($i == $pagination['currentPage'])
                <a href="?page={{ $i }}" class="pagination_cell inactive">{{ $i }}</a>
            @else
                <a href="?page={{ $i }}" class="pagination_cell">{{ $i }}</a>
            @endif
        </div>
    @endfor
    <div class="pagination_row_item">
        @if($pagination['currentPage'] == $pagination['totalPages'])
            <span class="pagination_cell inactive">Last &raquo;</span>
        @else
            <a href="?page={{ $pagination['totalPages'] }}" class="pagination_cell">Last &raquo;</a>
        @endif
    </div>
    <div style="clear: both;"></div>
</div>
