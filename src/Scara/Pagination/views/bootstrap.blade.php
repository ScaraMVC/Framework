<ul class="pagination">
    <!-- First Link -->
    @if($pagination['currentPage'] == 1)
        <li class="disabled"><a href="#">&laquo; First</a></li>
    @else
        <li><a href="?page=1">&laquo; First</a></li>
    @endif

    <!-- Middle Links (Numbered Pages) -->
    @for($i = 1; $i <= $pagination['totalPages']; $i++)
        @if($i == $pagination['currentPage'])
            <li class="disabled"><a href="?page={{ $i }}">{{ $i }}</a></li>
        @else
            <li><a href="?page={{ $i }}">{{ $i }}</a></li>
        @endif
    @endfor

    <!-- Last Link -->
    @if($pagination['currentPage'] == $pagination['totalPages'])
        <li class="disabled"><a href="#">Last &raquo;</a></li>
    @else
        <li><a href="?page={{ $pagination['totalPages'] }}">Last &raquo;</a></li>
    @endif
</ul>
