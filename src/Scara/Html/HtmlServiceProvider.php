<?php

namespace Scara\Html;

use Scara\Support\ServiceProvider;

/**
 * HTML/Form Service Provider.
 */
class HtmlServiceProvider extends ServiceProvider
{
    /**
     * Registers the HTML/Form Service Provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('html', function () {
            return new HtmlBuilder();
        });

        $this->create('form', function () {
            return new FormBuilder();
        });
    }
}
