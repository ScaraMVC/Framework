<?php

namespace Scara\Html;

use Scara\Utils\UrlGenerator;

/**
 * Standard HTML Form builder class.
 */
class FormBuilder extends BaseBuilder
{
    /**
     * URL Generator.
     *
     * @var \Scara\Utils\UrlGenerator
     */
    private $_generator;

    /**
     * Class Constructor // Creates new FormBuilder instance.
     */
    public function __construct()
    {
        $this->_generator = new UrlGenerator();
    }

    /**
     * Open a new form object.
     *
     * @param array $options - Form options
     *
     * @return string
     */
    public function open($options = [])
    {
        $options['method'] = self::getMethod($options);

        $options['action'] = self::getAction($options);

        $options['accept-charset'] = 'UTF-8';

        if (isset($options['files']) && $options['files']) {
            $options['enctype'] = 'multipart/form-data';
        }

        $options = $this->attributes($options);

        return '<form'.$options.'>';
    }

    /**
     * Creates a form label.
     *
     * @param string $for     - The ID of the form element the label should be for
     * @param string $content - The content of the label
     * @param array  $options - The label's options
     *
     * @return string
     */
    public function label($for, $content, $options = [])
    {
        return '<label for="'.$for.'"'.$this->attributes($options).'>'.$content.'</label>';
    }

    /**
     * Creates a form input element.
     *
     * @param string $type    - The type of form input
     * @param string $name    - The name of the input
     * @param string $value   - The value of the input
     * @param array  $options - The input's options
     *
     * @return string
     */
    public function input($type, $name, $value = '', $options = [])
    {
        if (!isset($options['id'])) {
            $options['id'] = $name;
        }

        if (!empty($value)) {
            $options['value'] = $value;
        }

        $options['type'] = $type;
        $options['name'] = $name;

        return '<input'.$this->attributes($options).'>';
    }

    /**
     * Creates a text form input box.
     *
     * @param string $name    - The text's name
     * @param string $value   - The text's value
     * @param array  $options - The text's options
     *
     * @return string
     */
    public function text($name, $value = '', $options = [])
    {
        return $this->input('text', $name, $value, $options);
    }

    /**
     * Creates an email form input box.
     *
     * @param string $name    - The email's name
     * @param string $value   - The email's value
     * @param array  $options - The email's options
     *
     * @return string
     */
    public function email($name, $value = '', $options = [])
    {
        return $this->input('email', $name, $value, $options);
    }

    /**
     * Creates a password form input box.
     *
     * @param string $name    - The password's name
     * @param string $value   - The password's value
     * @param array  $options - The password's options
     *
     * @return string
     */
    public function password($name, $value = '', $options = [])
    {
        return $this->input('password', $name, $value, $options);
    }

    /**
     * Creates a hidden form input.
     *
     * @param string $name    - The hidden field's name
     * @param string $value   - The hidden field's value
     * @param array  $options - The hidden field's options
     *
     * @return string
     */
    public function hidden($name, $value = '', $options = [])
    {
        return $this->input('hidden', $name, $value, $options);
    }

    /**
     * Creates a textarea form input box.
     *
     * @param string $name    - The textarea's name
     * @param string $value   - The textarea's value
     * @param array  $options - The textarea's options
     *
     * @return string
     */
    public function textarea($name, $value = '', $options = [])
    {
        if (!isset($options['id'])) {
            $options['id'] = $name;
        }

        $options['name'] = $name;

        return '<textarea'.$this->attributes($options).'>'.$value.'</textarea>';
    }

    /**
     * Creates a form submit button.
     *
     * @param string $value   - The submit button's value
     * @param array  $options - The submit button's options
     *
     * @return string
     */
    public function submit($value = '', $options = [])
    {
        return $this->input('submit', null, $value, $options);
    }

    /**
     * Creates a form reset button.
     *
     * @param string $value   - The reset button's value
     * @param array  $options - The reset button's options
     *
     * @return string
     */
    public function reset($value = '', $options = [])
    {
        return $this->input('reset', null, $value, $options);
    }

    /**
     * Closes the form.
     *
     * @return string
     */
    public function close()
    {
        return '</form>';
    }

    /**
     * Gets the form method.
     *
     * @param array $options - The form's options
     *
     * @return string
     */
    private function getMethod($options)
    {
        if (isset($options['method'])) {
            $method = $options['method'];
        } else {
            $method = 'POST';
        }

        return $method;
    }

    /**
     * Gets the form action.
     *
     * @param array $options - The form's options
     *
     * @return string
     */
    private function getAction($options)
    {
        if (isset($options['action'])) {
            $action = $this->_generator->to($options['action']);
        } else {
            $action = $_SERVER['REQUEST_URI'];
        }

        return $action;
    }

    /**
     * Gets an input's id, or sets it if not supplied.
     *
     * @param array $options - The input's options
     *
     * @deprecated
     *
     * @return string
     */
    private function getId($options)
    {
        $id = '';
        if (!isset($options['id'])) {
            $id = $options['name'];
        } else {
            $id = $options['id'];
        }

        return $id;
    }
}
