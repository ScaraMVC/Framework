<?php

namespace Scara\Utils;

use Scara\Config\Configuration;

/**
 * Used for making class aliases from supplied facades.
 */
class Aliases
{
    /**
     * Array of aliases provided by application config.
     *
     * @var array
     */
    private $_aliases;

    /**
     * Class Constructor // Sets aliases property to array from application config.
     */
    public function __construct()
    {
        $config = new Configuration();
        $this->_aliases = $config->from('app')->get('aliases');
    }

    /**
     * Registers supplies aliases and executes it's boot sequence if one is given in each alias.
     *
     * @return void
     */
    public function registerAliases()
    {
        foreach ($this->_aliases as $alias => $namespace) {
            class_alias($namespace, $alias);
        }
    }
}
