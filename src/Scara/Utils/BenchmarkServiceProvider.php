<?php

namespace Scara\Utils;

use Scara\Support\ServiceProvider;

class BenchmarkServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->create('benchmark', function () {
            return new Benchmark();
        });
    }
}
