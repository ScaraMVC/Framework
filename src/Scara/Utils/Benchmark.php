<?php

namespace Scara\Utils;

/**
 * Benchmarks the application.
 */
class Benchmark
{
    /**
     * Array of all benchmark values.
     *
     * @var array
     */
    public static $marks = [];

    /**
     * Starts a benchmark with a given name.
     *
     * @param string $name
     *
     * @return void
     */
    public function start($name)
    {
        static::$marks[$name] = microtime(true);
    }

    /**
     * Gets elapsed time in milliseconds for given benchmark.
     *
     * @param string $name
     *
     * @return float
     */
    public function check($name)
    {
        if (array_key_exists($name, static::$marks)) {
            return number_format((microtime(true) - static::$marks[$name]) * 1000, 2);
        }

        return 0.0;
    }

    /**
     * Gets the memory usage of the application in MB.
     *
     * @return float
     */
    public function memcheck()
    {
        return number_format(memory_get_usage() / 1024 / 1024, 2);
    }
}
