<?php

if (!function_exists('base_path')) {
    /**
     * Gets the applications root directory using getcwd()
     * Not to be confused with Config::get('basepath', 'app');.
     *
     * @return string
     */
    function base_path()
    {
        return getcwd();
    }
}

if (!function_exists('app_path')) {
    /**
     * Gets the application path.
     *
     * @return string
     */
    function app_path()
    {
        return getcwd().'/app';
    }
}

if (!function_exists('storage_path')) {
    /**
     * Gets the application's storage path.
     *
     * @return string
     */
    function storage_path()
    {
        return getcwd().'/storage';
    }
}

if (!function_exists('printr')) {
    /**
     * Uses PHP's print_r function wrapped around pre tags.
     *
     * @param mixed $object
     *
     * @return void
     */
    function printr($object)
    {
        echo '<pre>', print_r($object), '</pre>';
    }
}

if (!function_exists('vd')) {
    /**
     * Uses PHP's var_dump function wrapped around pre tags.
     *
     * @param mixed $object
     *
     * @return void
     */
    function vd($object)
    {
        echo '<pre>', var_dump($object), '</pre>';
    }
}

if (!function_exists('latest_version')) {
    /**
     * Gets latest version from packagist.
     *
     * @return string
     */
    function latest_version()
    {
        $url = 'https://packagist.org/packages/scaramvc/scara.json';
        $data = file_get_contents($url);
        $json = json_decode($data);

        foreach ($json->package->versions as $version) {
            if ($version->version !== 'dev-master') {
                return str_replace('v', '', $version->version);
            }
        }
    }
}

if (!function_exists('version')) {
    /**
     * Gets Scara's current version.
     *
     * @return string
     */
    function version()
    {
        return \Scara\Foundation\Application::$VERSION;
    }
}

if (!function_exists('config_path')) {
    /**
     * Gets the configuration path.
     *
     * @return string
     */
    function config_path()
    {
        return getcwd().'/config';
    }
}

if (!function_exists('config_from')) {
    /**
     * Helper function to load in a config file.
     *
     * @param string $file
     *
     * @return \Scara\Config\Configuration
     */
    function config_from($file)
    {
        return Config::from($file);
    }
}

if (!function_exists('config_get')) {
    /**
     * Helper function to get a config value.
     *
     * @param string $key
     * @param string $file
     *
     * @return mixed
     */
    function config_get($key, $file)
    {
        return Config::get($key, $file);
    }
}

if (!function_exists('asset')) {
    /**
     * Helper function to load in an asset.
     *
     * @param string $path
     *
     * @return string
     */
    function asset($path)
    {
        return Html::asset($path);
    }
}

if (!function_exists('env')) {
    /**
     * Gets an environment variable.
     *
     * @return string
     */
    function env($key, $default = '')
    {
        $e = new Scara\Foundation\Environment();

        return $e->get($key, $default);
    }
}

if (!function_exists('env_set')) {
    /**
     * Sets an environment variable in .env.
     *
     * @param string $key
     * @param string $value
     *
     * @return bool
     */
    function env_set($key, $value)
    {
        $e = new Scara\Foundation\Environment();

        return $e->set($key, $value);
    }
}

if (!function_exists('link_to')) {
    /**
     * Helper function to generate an HTML link.
     *
     * @param string $url
     * @param string $content
     * @param array  $options
     * @param bool   $safe
     *
     * @return string
     */
    function link_to($url, $content = '', $options = [], $safe = false)
    {
        return Html::link($url, $content, $options, $safe);
    }
}

if (!function_exists('script')) {
    /**
     * Helper function to generate a script tag.
     *
     * @param string $file
     *
     * @return string
     */
    function script($file)
    {
        return Html::script($file);
    }
}

if (!function_exists('stylesheet')) {
    /**
     * Helper function to generate a link tag for stylesheets.
     *
     * @param string $file
     *
     * @return string
     */
    function stylesheet($file)
    {
        return Html::stylesheet($file);
    }
}

if (!function_exists('url')) {
    /**
     * Generates a URL string.
     *
     * @param string $path
     * @param string
     */
    function url($path)
    {
        return Html::url($path);
    }
}

if (!function_exists('session_get')) {
    /**
     * Gets a value from the session.
     *
     * @param string $key - Session key
     *
     * @return mixed
     */
    function session_get($key)
    {
        return Session::get($key);
    }
}

if (!function_exists('session_set')) {
    /**
     * Adds to the session.
     *
     * @param string $key   - Key given to session
     * @param string $value - Value given to session's $key
     *
     * @return mixed
     */
    function session_set($key, $value = '')
    {
        return Session::set($key, $value);
    }
}

if (!function_exists('session_flash')) {
    /**
     * Creates a flash message by setting a session key and removing it upon being called again with the specified key.
     *
     * @param string $key   - Session key
     * @param string $value - Session $key's value. Calls session by $key if left empty
     *
     * @return mixed
     */
    function session_flash($key, $value = '')
    {
        return Session::flash($key, $value);
    }
}

if (!function_exists('session_has')) {
    /**
     * Checks the session has a specified key.
     *
     * @param string $key - Session key
     *
     * @return bool
     */
    function session_has($key)
    {
        return Session::has($key);
    }
}

if (!function_exists('session_delete')) {
    /**
     * Deletes a key from the session.
     *
     * @param string $key - Session key
     *
     * @return void
     */
    function session_delete($key)
    {
        return Session::delete($key);
    }
}
