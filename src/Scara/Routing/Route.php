<?php

namespace Scara\Routing;

/**
 * The route definition file.
 */
class Route
{
    /**
     * The request methods allowed in each route registration.
     *
     * @var array
     */
    private $_methods = ['GET', 'POST', 'PUT', 'DELETE'];

    /**
     * Array every route is registered to.
     *
     * @var array
     */
    private $_routes = [];

    /**
     * Registers each route.
     *
     * @param string $method - Route's request method
     * @param string $url    - Route's URL
     * @param mixed  $action - Route's action
     *
     * @throws \Exception
     *
     * @return void
     */
    public function register($method, $url, $action)
    {
        if (in_array($method, $this->_methods)) {
            if (is_array($action)) {
                if (!empty($action)) {
                    $exp = explode('@', $action['uses']);
                    $this->_routes[] = [
                        'controller' => $exp[0],
                        'action'     => $exp[1],
                        'method'     => $method,
                        'uri'        => $url,
                    ];
                } else {
                    throw new \Exception('Cannot leave the action empty!');
                }
            } else {
                $exp = explode('@', $action);
                $this->_routes[] = [
                    'controller' => $exp[0],
                    'action'     => $exp[1],
                    'method'     => $method,
                    'uri'        => $url,
                ];
            }
        } else {
            throw new \Exception('Invalid routing method!');
        }
    }

    /**
     * Gets all registered routes.
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->_routes;
    }
}
