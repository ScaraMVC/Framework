<?php

namespace Scara\Routing;

/**
 * Handles registering routes and requesting routes.
 */
class RouteLoader
{
    /**
     * The router.
     *
     * @var \Scara\Routing\Router
     */
    private $_router;

    /**
     * Class Constructor // Sets class's router instance to
     * provided router.
     *
     * @param \Scara\Routing\Router $router - The router
     *
     * @return void
     */
    public function registerRouter(Router $router)
    {
        $this->_router = $router;
    }

    /**
     * Gets the requested route for the controller.
     *
     * @param string $basepath - Application's base path
     *
     * @return array
     */
    public function getRequestedRoute($basepath)
    {
        if ($basepath == '/') {
            $url = strtolower($_SERVER['REQUEST_URI']);
        } else {
            $url = explode($basepath, strtolower($_SERVER['REQUEST_URI']))[1];
        }

        return $this->_router->getRoute($url);
    }
}
