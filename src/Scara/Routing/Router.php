<?php

namespace Scara\Routing;

use Config;

$router = ''; // Global router object for use in routes.php

/**
 * Parses routes.php and creates router for controllers.
 */
class Router
{
    /**
     * The route object.
     *
     * @var \Scara\Routing\Route
     */
    private $_route;

    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Handles registering defined routes.
     *
     * @return void
     */
    public function registerRoutes()
    {
        $this->_route = new Route();
        $this->_config = Config::from('app');

        $router = $this; // MUST KEEP!!!!!!!!!

        require_once $this->_config->from('app')->get('routes');
    }

    /**
     * Registers a GET request route.
     *
     * @param string $url    - The route's URL
     * @param mixed  $action - The route's action
     *
     * @return void
     */
    public function get($url, $action)
    {
        $this->_route->register('GET', $url, $action);
    }

    /**
     * Registers a POST request route.
     *
     * @param string $url    - The route's URL
     * @param mixed  $action - The route's action
     *
     * @return void
     */
    public function post($url, $action)
    {
        $this->_route->register('POST', $url, $action);
    }

    /**
     * Gets the route from supplied URL.
     *
     * @param string $url - The route's URL
     *
     * @return array
     */
    public function getRoute($url)
    {
        $routes = $this->_route->getRoutes();
        $return = '';
        foreach ($routes as $route) {
            $routeURI = $this->_config->from('app')->get('basepath').$route['uri'];

            // POST requests
            if (isset($_POST)) {
                foreach ($_POST as $key => $value) {
                    $route['query'][] = [$key => $value];
                }
            }

            // GET requests
            $getexp = explode('?', $_SERVER['REQUEST_URI']);
            if (isset($getexp[1])) {
                if (implode('?', $getexp) == $routeURI.'?'.$getexp[1]) {
                    $route['querystring'] = $getexp[1];
                    $qexp = explode('&', $route['querystring']);
                    foreach ($qexp as $q) {
                        $i = explode('=', $q);
                        $route['query'][] = [$i[0] => $i[1]];
                    }
                }
            }

            // For handling routes with variables
            $expressionExp = explode('/', $route['uri']);
            $serveruri = '/'.substr($_SERVER['REQUEST_URI'], strrpos($_SERVER['REQUEST_URI'], $expressionExp[1]));
            $serveruriexp = explode('/', $serveruri);

            for ($i = 0; $i < count($expressionExp); $i++) {
                $expressionExp[$i] = preg_replace_callback('/\{(.+)\}/', function ($m) use ($serveruriexp, $i, &$route) {
                    $route['query'][] = [$m[1] => $serveruriexp[$i]];

                    return strtolower($serveruriexp[$i]); // url hack to accept upper cases too
                }, $expressionExp[$i]);
            }

            $expression = implode('/', $expressionExp);
            $route['uri'] = $expression;

            if (isset($route['querystring'])) {
                $checkurl = $route['uri'].'?'.$route['querystring'];
            } else {
                $checkurl = $route['uri'];
            }

            if ((strtolower($checkurl) == $url) && ($_SERVER['REQUEST_METHOD'] == $route['method'])) {
                return $route;
            }
        }
    }
}
