<?php

namespace Scara\Hashing;

use Scara\Support\ServiceProvider;

/**
 * BCryptHasher service provider.
 */
class HashServiceProvider extends ServiceProvider
{
    /**
     * Registers the Hashing service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('hash', function () {
            return new BcryptHasher();
        });
    }
}
