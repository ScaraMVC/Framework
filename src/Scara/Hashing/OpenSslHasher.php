<?php

namespace Scara\Hashing;

use Scara\Config\Configuration;

/**
 * Hashes a string using PHP's OpenSSL extension.
 */
class OpenSslHasher
{
    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration();
        $this->_config = $c->from('app');
    }

    /**
     * Generates a random string based on given length.
     *
     * Blatantly taken from StackOverflow: http://stackoverflow.com/a/13212994/1524676
     *
     * @param int $length
     *
     * @return string
     */
    public function random_string($length = 32)
    {
        return substr(str_shuffle(str_repeat(
            $x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            ceil($length / strlen($x))
        )), 1, $length);
    }

    /**
     * Creates an IV for generating an OpenSSL hashed value.
     *
     * @return string
     */
    public function createiv()
    {
        return $this->random_string(16);
    }

    /**
     * Generate a token using OpenSSL extensions.
     *
     * @param string $iv
     * @param string $string
     *
     * @return string
     */
    public function tokenize($iv, $string = '')
    {
        if (empty($string)) {
            $string = $this->random_string();
        }

        return openssl_encrypt($string, 'AES-256-CBC', $this->_config->get('token'), 0, $iv);
    }

    /**
     * Decrypt OpenSSL token. Requires tokens used to generate token you're attempting to decrypt.
     *
     * @param string $iv
     * @param string $string
     *
     * @return string
     */
    public function untokenize($iv, $string)
    {
        return openssl_decrypt($string, 'AES-256-CBC', $this->_config->get('token'), 0, $iv);
    }
}
