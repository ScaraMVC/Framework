<?php

namespace Scara\Hashing;

/**
 * Hashes strings using BCrypt.
 */
class BcryptHasher
{
    /**
     * Hash a string.
     *
     * @param string $string
     * @param int    $cost
     *
     * @return string
     */
    public function make($string, $cost = 10)
    {
        return password_hash($string, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    /**
     * Check if a hash matches a given string.
     *
     * @param string $string
     * @param string $hash
     *
     * @return bool
     */
    public function check($string, $hash)
    {
        return password_verify($string, $hash);
    }

    /**
     * Check if a hash needs to be rehashed.
     *
     * @param string $hash
     * @param int    $cost
     *
     * @return bool
     */
    public function needsRehashing($hash, $cost = 10)
    {
        return password_needs_rehash($hash, PASSWORD_BCRYPT, ['cost' => $cost]);
    }
}
