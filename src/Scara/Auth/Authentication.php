<?php

namespace Scara\Auth;

use Scara\Config\Configuration;
use Scara\Database\Database;
use Scara\Hashing\OpenSslHasher;
use Scara\Session\SessionInitializer;

/**
 * Handles user authentication.
 */
class Authentication
{
    /**
     * Session/Cookie instance.
     *
     * @var mixed
     */
    private $_session;

    /**
     * App config instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * OpenSSL Hashing instance.
     *
     * @var \Scara\Hashing\OpenSslHasher
     */
    private $_ssl;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration();
        $this->_config = $c->from('app');
        $this->_session = SessionInitializer::init();
        $this->_ssl = new OpenSslHasher();
    }

    /**
     * Checks if a user is authenticated.
     *
     * @return bool
     */
    public function check()
    {
        if ($this->_session->has('auth')) {
            // They have an auth, let's attempt to check expiration
            // and master session
            $s = [
                'session' => $this->_session->get('SCARA_SESSION'),
                'token'   => $this->_session->get('SCARA_SESSION_TOKEN'),
            ];

            $ua = $this->_ssl->untokenize($s['token'], $s['session']);

            if ($ua === hash('sha256', $_SERVER['HTTP_USER_AGENT'])) {
                $expire = $this->_session->get('expire');
                if (time() < $expire) {
                    return true;
                } else {
                    $this->logout();
                }
            }
        }

        return false;
    }

    /**
     * Attempts to authenticate/login a user.
     *
     * @param array $options - The login credentials
     *
     * @return bool
     */
    public function attempt($options = [])
    {
        if (empty($options)) {
            throw new \Exception('You must provide the login credentials!');
        }

        $db = new Database();
        $cap = $db->getCapsule();

        $table = $cap->table($this->_config->get('auth_table'));

        $keys = [];

        foreach ($options as $key => $value) {
            $keys[] = $key;
        }

        $user = $table->where($keys[0], '=', $options[$keys[0]]);

        if ($user->count() > 0) {
            $user = $user->first();
            $keys1 = $keys[1];
            if ($user->$keys1 == $options[$keys1]) {
                $iv = $this->_ssl->random_string(16);
                $this->_session->set('_token', $this->_ssl->tokenize($iv));
                $this->_session->set('auth', $this->_ssl->tokenize($iv, $user->id));
                $this->_session->set('expire', time() + (((60 * 60) * 24) * 30));
                $this->_session->set('_token_meta', $iv);

                return true;
            }
        }

        return false;
    }

    /**
     * Gets the currently authenticated user.
     *
     * @return mixed
     */
    public function user()
    {
        if ($this->check()) {
            $db = new Database();
            $cap = $db->getCapsule();

            $idt = $this->_session->get('auth');
            $iv = $this->_session->get('_token_meta');
            $id = $this->_ssl->untokenize($iv, $idt);

            $table = $cap->table($this->_config->get('auth_table'));

            return $table->find($id);
        } else {
            return false;
        }
    }

    /**
     * Logs the authenticated user out.
     *
     * @return bool
     */
    public function logout()
    {
        $this->_session->delete('auth');
        $this->_session->delete('expire');
        $this->_session->delete('_token');
        $this->_session->delete('_token_meta');

        return $this->check() ? false : true;
    }
}
