<?php

namespace Scara\Auth;

use Scara\Support\ServiceProvider;

/**
 * Authentication Service Provider.
 */
class AuthenticationServiceProvider extends ServiceProvider
{
    /**
     * Registers the Authentication's service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('authentication', function () {
            return new Authentication();
        });
    }
}
