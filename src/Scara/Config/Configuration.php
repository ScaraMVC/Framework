<?php

namespace Scara\Config;

/**
 * Application Configuration class.
 */
class Configuration
{
    /**
     * File to load.
     *
     * @var string
     */
    public $_configFile;

    /**
     * What file to load the config from.
     *
     * @param string $file - The name of the config file to load from
     *
     * @return \Scara\Config\Configuration
     */
    public function from($file)
    {
        $this->_configFile = $file;

        return $this;
    }

    /**
     * Gets the config value.
     *
     * @param string $key        - The config item's key
     * @param string $configFile - Optional if you want to load a specific config file
     *
     * @return mixed
     */
    public function get($key, $configFile = '')
    {
        $config = $this->load((empty($configFile)) ? $this->_configFile : $configFile);

        return $this->sift($config, explode('.', $key));
    }

    /**
     * Loads a configuration script.
     *
     * @param string $file - The name of the config file to load
     * @param string $dir  - The sub directory to load into
     *
     * @return mixed
     */
    private function load($file, $dir = '')
    {
        $path = config_path().'/'.$dir;
        $handle = opendir($path);
        $ignore = ['.', '..'];
        while ($f = readdir($handle)) {
            if (!in_array($f, $ignore)) {
                if (is_dir($path.$f)) {
                    if (file_exists($path.$f.'/'.$file.'.php')) {
                        return $this->load($file, $f);
                    }
                } else {
                    $filename = explode('.', $f)[0];
                    if ($filename == $file) {
                        return require $path.'/'.$f;
                    }
                }
            }
        }
        closedir($handle);

        return false;
    }

    /**
     * Sifts through given items to load config arrays/items.
     *
     * @param mixed $config
     * @param mixed $items
     *
     * @return mixed
     */
    private function sift($config, $items)
    {
        foreach ($items as $item) {
            if (is_array($config[$item])) {
                if (count($items) > 1) {
                    array_shift($items);

                    return $this->sift($config[$item], $items);
                }

                return $config[$item];
            } else {
                return $config[$item];
            }
        }
    }
}
