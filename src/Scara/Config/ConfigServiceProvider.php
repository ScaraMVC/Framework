<?php

namespace Scara\Config;

use Scara\Support\ServiceProvider;

/**
 * Configuration Service Provider.
 */
class ConfigServiceProvider extends ServiceProvider
{
    /**
     * Registers the configuration's service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->create('config', function () {
            return new Configuration();
        });
    }
}
