<?php

namespace Scara\Support;

/**
 * Base service provider for creating facade service providers.
 */
abstract class ServiceProvider
{
    /**
     * The name of the provider.
     *
     * @var string
     */
    private $_name;

    /**
     * The provider class object.
     *
     * @var object
     */
    private $_class;

    /**
     * Registers a service provider.
     *
     * @return void
     */
    abstract public function register();

    /**
     * Creates the provider and registers the provider to the global area.
     *
     * @param string   $name     - The name of the service provider
     * @param \Closure $callback - The callback for calling the provider's given class
     *
     * @return void
     */
    public function create($name, $callback)
    {
        $this->_name = $name;
        $this->_class = $callback();

        $GLOBALS['providers'][$this->_name] = $this->_class;
    }
}
