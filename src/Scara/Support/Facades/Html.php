<?php

namespace Scara\Support\Facades;

/**
 * HtmlBuilder's facade.
 */
class Html extends Facade
{
    /**
     * Registers the HTML facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'html';
    }
}
