<?php

namespace Scara\Support\Facades;

/**
 * Environment's facade.
 */
class Environment extends Facade
{
    /**
     * Registers the Environment facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'environment';
    }
}
