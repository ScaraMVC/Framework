<?php

namespace Scara\Support\Facades;

/**
 * Config's facade.
 */
class Config extends Facade
{
    /**
     * Registers the Config facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'config';
    }
}
