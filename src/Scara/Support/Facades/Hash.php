<?php

namespace Scara\Support\Facades;

/**
 * BCryptHasher's facade.
 */
class Hash extends Facade
{
    /**
     * Registers the Hash facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'hash';
    }
}
