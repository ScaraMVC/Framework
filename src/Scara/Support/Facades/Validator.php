<?php

namespace Scara\Support\Facades;

/**
 * Validation's facade.
 */
class Validator extends Facade
{
    /**
     * Registers the Validator facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'validator';
    }
}
