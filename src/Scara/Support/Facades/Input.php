<?php

namespace Scara\Support\Facades;

/**
 * Input's facade.
 */
class Input extends Facade
{
    /**
     * Registers the Input facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'input';
    }
}
