<?php

namespace Scara\Support\Facades;

/**
 * Session's facade.
 */
class Session extends Facade
{
    /**
     * Registers the Session facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'session';
    }
}
