<?php

namespace Scara\Support\Facades;

/**
 * Authentication class's facade.
 */
class Auth extends Facade
{
    /**
     * Registers the Auth facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'authentication';
    }
}
