<?php

namespace Scara\Support\Facades;

/**
 * Facade master class.
 */
class Facade
{
    /**
     * Gets the facade's accessor name.
     *
     * @throws \RuntimeException
     *
     * @deprecated
     *
     * @return string
     */
    protected static function getAccessor()
    {
        throw new \RuntimeException('Not implemented');
    }

    /**
     * Gets the facade's accessor name.
     *
     * @throws \RuntimeException
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return static::getAccessor();
    }

    /**
     * Gets the facade's root address.
     *
     * @return object
     */
    public static function getFacadeRoot()
    {
        return static::resolveFacadeInstance(static::getFacadeAccessor());
    }

    /**
     * Gets the requested facade instance from the providers.
     *
     * @return object
     */
    protected static function resolveFacadeInstance($name)
    {
        return $GLOBALS['providers'][$name];
    }

    /**
     * Handles calling facades statically.
     *
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        $instance = static::getFacadeRoot();

        switch (count($args)) {
            case 0:
            return $instance->$method();
            case 1:
            return $instance->$method($args[0]);
            case 2:
            return $instance->$method($args[0], $args[1]);
            case 3:
            return $instance->$method($args[0], $args[1], $args[2]);
            case 4:
            return $instance->$method($args[0], $args[1], $args[2], $args[3]);
            default:
            return call_user_func_array([$instance, $method], $args);
        }
    }
}
