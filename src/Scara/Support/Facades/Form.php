<?php

namespace Scara\Support\Facades;

/**
 * FormBuilder's facade.
 */
class Form extends Facade
{
    /**
     * Registers the Form facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'form';
    }
}
