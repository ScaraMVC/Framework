<?php

namespace Scara\Support\Facades;

/**
 * Benchmark class's facade.
 */
class Benchmark extends Facade
{
    /**
     * Registers the Benchmark facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'benchmark';
    }
}
