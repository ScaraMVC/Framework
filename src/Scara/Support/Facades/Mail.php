<?php

namespace Scara\Support\Facades;

/**
 * Mailer class's facade.
 */
class Mail extends Facade
{
    /**
     * Registers the Mailer facade accessor.
     *
     * @return string
     */
    protected static function getAccessor()
    {
        return 'mail';
    }
}
