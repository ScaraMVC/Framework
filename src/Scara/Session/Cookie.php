<?php

namespace Scara\Session;

/**
 * Handles cookies.
 */
class Cookie implements ISession
{
    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        if ($this->has($key)) {
            return unserialize($_COOKIE[$key]);
        } else {
            return false;
        }
    }

    /**
     * Adds to the session.
     *
     * @param string $key    - Key given to session
     * @param string $value  - Value given to session's $key
     * @param mixed  $expire - Sets expiration of cookie
     * @param stirng $path   - The path the cookie should be set to
     *
     * @return mixed
     */
    public function set($key, $value, $expire = 3600, $path = '/')
    {
        if ($expire == false) {
            setcookie($key, serialize($value), false, $path);
        } else {
            setcookie($key, serialize($value), time() + $expire, $path);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return (isset($_COOKIE[$key])) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        if ($this->has($key)) {
            unset($_COOKIE[$key]);
            setcookie($key, '', time() - 3600, '/');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function flash($key, $value = '')
    {
        if (!empty($value)) {
            $this->set($key, $value, false);
        } else {
            if ($this->has($key)) {
                $s = $this->get($key);
                $this->delete($key);

                return $s;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function input($key)
    {
        return $this->flash('input.'.$key);
    }
}
