<?php

namespace Scara\Session;

use Scara\Support\ServiceProvider;

/**
 * Session Service provider.
 */
class SessionServiceProvider extends ServiceProvider
{
    /**
     * Registers the session/cookie service provider.
     *
     * @return void
     */
    public function register()
    {
        // We'll load in the session initializer
        // and let it handle loading the correct module
        $this->create('session', function () {
            return SessionInitializer::init();
        });
    }
}
