<?php

namespace Scara\Session;

/**
 * Interface used for \Session and \Cookie.
 */
interface ISession
{
    /**
     * Gets a value from the session.
     *
     * @param string $key - Session key
     *
     * @return mixed
     */
    public function get($key);

    /**
     * Adds to the session.
     *
     * @param string $key   - Key given to session
     * @param string $value - Value given to session's $key
     *
     * @return mixed
     */
    public function set($key, $value);

    /**
     * Checks the session has a specified key.
     *
     * @param string $key - Session key
     *
     * @return bool
     */
    public function has($key);

    /**
     * Deletes a key from the session.
     *
     * @param string $key - Session key
     *
     * @return void
     */
    public function delete($key);

    /**
     * Creates a flash message by setting a session key and removing it upon being called again with the specified key.
     *
     * @param string $key   - Session key
     * @param string $value - Session $key's value. Calls session by $key if left empty
     *
     * @return mixed
     */
    public function flash($key, $value = '');

    /**
     * Gets input from session.
     *
     * @param $key - Session key
     *
     * @return mixed
     */
    public function input($key);
}
