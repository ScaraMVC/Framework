<?php

namespace Scara\Session;

use Scara\Config\Configuration;

/**
 * File based sessions.
 */
class File implements ISession
{
    /**
     * Configuration instance.
     *
     * @var \Scara\Config\Configuration
     */
    private $_config;

    /**
     * Class constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $c = new Configuration();
        $location = $c->from('app')->get('session_location');
        $this->_config = $location.hash('sha1', serialize($_SERVER['REMOTE_ADDR']));

        if (!file_exists($this->_config)) {
            $f = fopen($this->_config, 'w');
            fclose($f);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        $content = $this->content();
        if ($this->has($key)) {
            return unserialize($content->$key);
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        $content = $this->content();
        if (!is_null($content)) {
            $content->$key = serialize($value);
        } else {
            $content = [];
            $content[$key] = serialize($value);
        }
        $this->write($content);
    }

    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        $content = $this->content();

        return (isset($content->$key)) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        if ($this->has($key)) {
            $content = $this->content();
            unset($content->$key);
            $this->write($content);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function flash($key, $value = '')
    {
        if (!empty($value)) {
            $this->set($key, $value);
        } else {
            $content = $this->content();
            if ($this->has($key)) {
                $s = unserialize($content->$key);
                $this->delete($key);

                return $s;
            }
        }
    }

    /**
     * Gets input from session.
     *
     * @param $key - Session key
     *
     * @return mixed
     */
    public function input($key)
    {
        return $this->flash('input.'.$key);
    }

    /**
     * Gets the file session's content.
     *
     * @return mixed
     */
    private function content()
    {
        $handle = fopen($this->_config, 'r');
        $content = @json_decode(stream_get_contents($handle));

        return $content;
    }

    /**
     * Writes session info to file session.
     *
     * @param mixed $content
     *
     * @return void
     */
    private function write($content)
    {
        $handle = fopen($this->_config, 'w+');
        fwrite($handle, @json_encode($content));
        fclose($handle);
    }
}
