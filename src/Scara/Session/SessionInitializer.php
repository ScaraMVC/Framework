<?php

namespace Scara\Session;

use Scara\Config\Configuration;

/**
 * Initializes session handler.
 */
class SessionInitializer
{
    /**
     * Initializes session handler.
     *
     * @return mixed
     */
    public static function init()
    {
        $c = new Configuration();
        $cc = $c->from('app')->get('session');

        if (strtolower($cc) == 'session') {
            return new Session();
        } elseif (strtolower($cc) == 'file') {
            return new File();
        } elseif (strtolower($cc) == 'cookie') {
            return new Cookie();
        } else {
            throw new \Exception($cc.' is not a supported session type!');
        }
    }
}
