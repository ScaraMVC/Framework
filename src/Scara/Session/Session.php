<?php

namespace Scara\Session;

/**
 * Handles sessions.
 */
class Session implements ISession
{
    /**
     * {@inheritdoc}
     */
    public function has($key)
    {
        return (isset($_SESSION[$key])) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function set($key, $value)
    {
        return $_SESSION[$key] = serialize($value);
    }

    /**
     * {@inheritdoc}
     */
    public function get($key)
    {
        return unserialize($_SESSION[$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function delete($key)
    {
        if ($this->has($key)) {
            unset($_SESSION[$key]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function flash($key, $value = '')
    {
        if (!empty($value)) {
            $this->set($key, $value);
        } else {
            if ($this->has($key)) {
                $s = $this->get($key);
                $this->delete($key);

                return $s;
            }
        }
    }

    /**
     * Sets a session flash for redirection data.
     *
     * @param string $key   - Session key
     * @param string $value - Session $key's value. Calls session by $key if left empty
     *
     * @deprecated
     *
     * @return void
     */
    public function flashData($key, $value)
    {
        $$key = $key;
        $this->flash($$key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function input($key)
    {
        return $this->flash('input.'.$key);
    }

    /**
     * Gets entire session array.
     *
     * @return array
     */
    public function all()
    {
        return $_SESSION;
    }
}
