#!/bin/bash

git subsplit init git@github.com:scaramvc/framework.git
git subsplit publish --heads="master" src/Scara/Mail:git@github.com:scaramvc/mail.git
rm -rf .subsplit
